class Validators {
  static String validateMobile(String value) {
    String pattern = r'(^[0-9]*$)';
    RegExp regExp = new RegExp(pattern);
    if (value.replaceAll(" ", "").isEmpty) {
      return 'Mobile is required';
    } else if (value.replaceAll(" ", "").length != 10) {
      return 'Mobile number must 10 digits';
    } else if (!regExp.hasMatch(value.replaceAll(" ", ""))) {
      return 'Mobile number must be digits';
    }
    return null;
  }

  static String validUserName(String value) {
    String pattern = r'(^[a-zA-Z ]*$)';
    RegExp regExp = new RegExp(pattern);
    if (value.isEmpty) {
      return 'Username is required';
    } else if (!regExp.hasMatch(value)) {
      return 'Username must be a-z and A-Z';
    }
    return null;
  }

  static bool validateEmail(String value) {
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(pattern);
    if (value.isEmpty) {
      return false;
    } else if (!regExp.hasMatch(value)) {
      return false;
    } else {
      return true;
    }
  }

  static bool validateName(String value) {
    if (value.isEmpty) {
      return false;
    } else if (value.length < 2) {
      return false;
    } else {
      return true;
    }
  }

  static bool validateField(String value) {
    if (value.isEmpty) {
      return false;
    }  else {
      return true;
    }
  }


  static bool validatePhone(String phone) {
    if (phone.isEmpty) {
      return false;
    } else if (phone.length != 10) {
      return false;
    } else {
      return true;
    }
  }



  static bool validatePassword(String value) {
    if (value.isEmpty) {
      return false;
    } else if (value.length < 6 || value.length > 15 ) {
      return false;
    }
    return true;
  }

  static bool validateConfirmPassword(String value) {
    if (value.isEmpty) {
      return false;
    } else if (value.length < 4) {
      return false;
    }
    return true;
  }

  static bool validateOtp(String value) {
    if (value.isEmpty) {
      return false;
    } else if (value.length < 4) {
      return false;
    }
    return true;
  }

  static bool validationEqual(String currentValue, String checkValue) {
    if (currentValue == checkValue) {
      return true;
    } else {
      return false;
    }
  }

  static String address(String value) {
    if (value.isEmpty) {
      return 'Address is required';
    }
    return null;
  }

  static String state(String value) {
    if (value.isEmpty) {
      return 'State is required';
    }
    return null;
  }

  static String flatNoHouseNo(String value) {
    if (value.isEmpty) {
      return 'Flat No. / House No. is required';
    }
    return null;
  }

  static String city(String value) {
    if (value.isEmpty) {
      return 'City is required';
    }
    return null;
  }


  //validation for email
  static String validateEmailOld(String email) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (email.isEmpty)
      return 'Email can\'t be empty';
    else if (!regex.hasMatch(email))
      return 'Enter valid email address';
    else
      return null;
  }


  static bool validateUsername(String value){
    String  pattern = r'^[a-zA-Z]+(\s[a-zA-Z]+)?$';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(value);
  }


  static String validateMobileOld(String value) {
    String patttern =  r'(^[0-9]*$)';
    RegExp regExp = new RegExp(patttern);
    if (!regExp.hasMatch(value)) {
      return 'Invalid';
    }
    return "Valid";
  }

  static bool validatePasswordOld(String value){
    String  pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{2,}$';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(value);
  }

  static bool isEmail(String em) {
    String emailRegexp =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

    RegExp regExp = RegExp(emailRegexp);

    return regExp.hasMatch(em);
  }

}