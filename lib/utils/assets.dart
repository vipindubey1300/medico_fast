import 'enumeration.dart';

class AssetManager extends Enum<String> {

  AssetManager(String value) : super(value);

  static  AssetManager backgroundImage = AssetManager('assets/images/background.png');
  static  AssetManager logo = AssetManager('assets/images/logo.png');
  static  AssetManager hospital = AssetManager('assets/images/hospital.png');
  static  AssetManager google = AssetManager('assets/images/google.png');
  static  AssetManager fb = AssetManager('assets/images/fb.png');
  static  AssetManager mail = AssetManager('assets/images/mail.png');
  static  AssetManager phoneView = AssetManager('assets/images/phoneview.png');
  static  AssetManager category = AssetManager('assets/images/category.png');
  static  AssetManager bottle = AssetManager('assets/images/bottle.png');
  static  AssetManager heart = AssetManager('assets/images/heart.png');
  static  AssetManager flask = AssetManager('assets/images/flask.png');



  //extra
  static  AssetManager medicine = AssetManager('assets/images/extra/medicine.png');
  static  AssetManager ayurvedic = AssetManager('assets/images/extra/ayurvedic.png');
  static  AssetManager lab = AssetManager('assets/images/extra/lab.png');
  static  AssetManager martha = AssetManager('assets/images/extra/martha.png');
  static  AssetManager product = AssetManager('assets/images/extra/product.png');
  static  AssetManager banner = AssetManager('assets/images/extra/banner.png');
  static  AssetManager banner2 = AssetManager('assets/images/extra/banner2.png');


}
