import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/utility.dart';


abstract class AppStyles {


  static const BorderSide _customBorderSide =  BorderSide(width: 0.6, color:Colors.grey,);
  static const  BoxDecoration boxDecoration = const BoxDecoration(
    border: Border(
      bottom:_customBorderSide,
      top: _customBorderSide,
      left:_customBorderSide,
      right:_customBorderSide,
    ),
    borderRadius: BorderRadius.all( Radius.circular(8.0)),
    color: Colors.transparent,
    boxShadow: [
      BoxShadow(
        color:Colors.transparent,
        blurRadius: 2.0,
        spreadRadius: 0.0,
       // offset: Offset(2.0, 2.0), // shadow direction: bottom right
      )
    ],

  );






  static const TextStyle appNameStyle = TextStyle(
      fontFamily: 'Montserrat',
      color: AppColors.PRIMARY_COLOR,
      fontSize: 32,
      height: 1,
      fontWeight: FontWeight.w800
  );


  static const TextStyle headerTextStyle = TextStyle(
      fontFamily: 'Montserrat',
      color: AppColors.PRIMARY_COLOR,
      fontSize: 25,
      height: 1,
      fontWeight: FontWeight.w700
  );


  static const TextStyle smallTextStyle = TextStyle(
      fontFamily: 'Montserrat',
      color: AppColors.BLACK_TEXT_COLOR,
      fontSize: 14,
      height: 1.4,
      fontWeight: FontWeight.w600
  );

  static const TextStyle mediumTextStyle = TextStyle(
      fontFamily: 'Montserrat',
      color: AppColors.BLACK_TEXT_COLOR,
      fontSize: 17,
      height: 1,
      fontWeight: FontWeight.w600
  );

  static const whiteBoldText = TextStyle(color: Colors.white,fontWeight: FontWeight.w800,fontSize: 15);
  static const whiteMediumText = TextStyle(color: Colors.white,fontWeight: FontWeight.w600,fontSize: 18);
  static const whiteText = TextStyle(color: Colors.white,fontWeight: FontWeight.w500,fontSize: 13);
  static const greyText = TextStyle(color: Colors.grey,fontWeight: FontWeight.w500,fontSize: 13);
  static const greenText = TextStyle(color: AppColors.PRIMARY_COLOR,fontWeight: FontWeight.w400,fontSize: 15);
  static const greenTextBold = TextStyle(color: AppColors.PRIMARY_COLOR,fontWeight: FontWeight.w600,fontSize: 16);
  static const blackText = TextStyle(color: Colors.black,fontWeight: FontWeight.w400,fontSize: 15);
  static const blackBoldText = TextStyle(color: Colors.black,fontWeight: FontWeight.w700,fontSize: 15);
  static const blackMediumText = TextStyle(color: Colors.black,fontWeight: FontWeight.w600,fontSize: 17);
  static const errorText = TextStyle(color: Colors.red,fontWeight: FontWeight.w400,fontSize: 13);
  static const errorBigText = TextStyle(color: Colors.red,fontWeight: FontWeight.w500,fontSize: 15);





  static InputDecoration textFieldStyle({String labelTextStr="",String hintTextStr=""})   =>  InputDecoration(
        contentPadding: EdgeInsets.all(12),
        labelText: labelTextStr,
        hintText:hintTextStr,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      );


}