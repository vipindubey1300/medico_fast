import 'package:flutter/services.dart';
//import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/material.dart';
import 'package:medico_fast/utils/app_colors.dart';

import 'dart:async';
import 'dart:ui';
import 'package:medico_fast/utils/utility.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Utility {

  //show message in application
  static void showMessage(String message,{bool error = true}){
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
       // gravity: ToastGravity.BOTTOM,
        backgroundColor: error ? Colors.red : AppColors.PRIMARY_COLOR,
        textColor:  error ? Colors.white :Colors.black,
        fontSize: 16.0
    );
  }


  //get device width
  static double getDeviceWidth(BuildContext context){
    return MediaQuery.of(context).size.width;
  }

  //get device width
  static double getDeviceHeight(BuildContext context){
    return MediaQuery.of(context).size.height;
  }

//show loading progress bar
  static showLoadingProgress(BuildContext context){
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(margin: EdgeInsets.only(left: 5),child:Text("Loading..." )),
        ],),
    );
    showDialog(barrierDismissible: false,
      context:context,
      builder:(BuildContext context){
        return alert;
      },
    );
  }


  static void setStatusBar(){
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Colors.white, // this one for android
        statusBarBrightness: Brightness.light// this one for iOS
    ));
  }


  //use of print the logs as print() truncate long text while test
  static void printWrapped(String text) {
    final pattern = RegExp('.{1,800}'); // 800 is the size of each chunk
    pattern.allMatches(text).forEach((match) => print(match.group(0)));
  }

    /// Determine the current position of the device.
  ///
  /// When the location services are not enabled or permissions
  /// are denied the `Future` will return an error.
  static Future<Position> getCurrentPosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }


    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission != LocationPermission.whileInUse &&
          permission != LocationPermission.always) {
        return Future.error(
            'Location permissions are denied (actual value: $permission).');
      }
    }



    return await Geolocator.getCurrentPosition();
  }

  static Future<String> getCurrentAddress(Position position) async{
    String address = "";
    final coordinates = new Coordinates(position.latitude,position.longitude);
    var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    address = '${first.locality}, ${first.adminArea},${first.subLocality}, ${first.subAdminArea},${first.addressLine},'
        ' ${first.featureName},${first.thoroughfare}, ${first.subThoroughfare}';
    return address;
  }

}
