import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/styles.dart';
import 'package:medico_fast/utils/utility.dart';
import 'package:medico_fast/widgets/touchable_widget.dart';

class BottomView extends StatelessWidget {
  final GestureTapCallback onLeftPressed;
  final GestureTapCallback onRightPressed;
  final String leftText;
  final String rightText;
  final IconData leftIcon;
  final IconData rightIcon;


  BottomView({
    @required this.leftText  ,
    @required this.rightText  ,
    @required this.leftIcon  ,
    @required this.rightIcon  ,
    this.onLeftPressed = _onLeftPressed ,//if no handler provided then call this fucntiona
    this.onRightPressed = _onRightPressed
  });


  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 0,left: 0,right:0 ,
      child: Container(
        height: 60,
        width: Utility.getDeviceWidth(context) ,
        color: AppColors.PRIMARY_COLOR,
        padding: EdgeInsets.all(10),
        child:  Row(
          children: [
            Expanded(
              flex: 1,
              child:TouchableWidget(
                onTap: () => onLeftPressed,
                child:  Center(
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(leftIcon,color: Colors.white,),
                      SizedBox(width: 7,),
                      Text(leftText,style: AppStyles.whiteMediumText,)
                    ],
                  ),
                ),
              ),
            ),
            VerticalDivider(color: Colors.white,thickness: 1,),
            Expanded(
              flex: 1,
              child: TouchableWidget(
                onTap: () => onRightPressed,
                child: Center(
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(rightIcon,color: Colors.white),
                      SizedBox(width: 7,),
                      Text(rightText,style: AppStyles.whiteMediumText)
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  static dynamic _onLeftPressed() {}
  static dynamic _onRightPressed() {}
}



