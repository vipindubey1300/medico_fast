import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:medico_fast/models/address.dart';
import 'package:medico_fast/models/medicine.dart';
import 'package:medico_fast/models/orders.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/styles.dart';
import 'package:medico_fast/utils/utility.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:medico_fast/widgets/custom_button.dart';
import 'package:medico_fast/widgets/custom_text_field.dart';


class AddressCard extends StatefulWidget {
  final Address address;


  AddressCard({  Key key, this.address,}) : super(key: key);

  _AddressCardState createState() => _AddressCardState();
}

class _AddressCardState extends State<AddressCard> {

  void addressEditModal() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor:  AppColors.SECONDARY_COLOR,
            contentPadding: EdgeInsets.all(0),
            content:Container(
              height: Utility.getDeviceHeight(context) * 0.5,
              width: Utility.getDeviceWidth(context) * 0.4,
              child:  Stack(
                overflow: Overflow.visible,
                fit: StackFit.expand,
                children: <Widget>[
                   Card(

                    color: AppColors.SECONDARY_COLOR,
                    child:SingleChildScrollView(
                      child:  Container(
                        padding: EdgeInsets.all(10),
                        child: Column(
                          children: [
                            CustomTextField(
                              label: 'Name',
                            ),
                            SizedBox(height: 10,),
                            CustomTextField(
                              label: 'Phone Number',
                            ),
                            SizedBox(height: 10,),
                            CustomTextField(
                              label: 'Full Address',
                              inputType: TextInputType.multiline,

                            ),
                            SizedBox(height: 20,),
                            CustomButton(text: 'Edit Address')
                          ],
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    right: -20.0,
                    top: -20.0,
                    child: InkResponse(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: CircleAvatar(
                        child: Icon(Icons.close),
                        backgroundColor: AppColors.PRIMARY_COLOR,
                        foregroundColor: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }


  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.all(10),
        color: Colors.white,
        child:Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Martha kent",style: AppStyles.blackBoldText,),
                IconButton(icon: Icon(Icons.edit), onPressed:(){addressEditModal();},color: AppColors.PRIMARY_COLOR, )
              ],
            ),
            SizedBox(height: 10,),
            Text(widget.address.address,style: AppStyles.blackText),
            SizedBox(height: 10,),
            Text(widget.address.phone,style: AppStyles.blackText)
          ],
        ) // just for the demo, you can pass optionsChoices()
    );
  }
}
