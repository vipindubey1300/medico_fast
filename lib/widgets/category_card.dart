import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:medico_fast/models/category.dart';
import 'package:medico_fast/models/medicine.dart';
import 'package:medico_fast/screens/products.dart';
import 'package:medico_fast/utils/constants.dart';
import 'package:medico_fast/utils/utility.dart';




class CategoryCard extends StatelessWidget {

  final CategoryModel categoryModel;

  CategoryCard( {
    Key key,
    this.categoryModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.of(context, rootNavigator: true)
          .push(MaterialPageRoute(builder: (context) => new Products())),
      child: Card(
        elevation: 6,
        margin: EdgeInsets.all(10),
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: Container(
            width: Utility.getDeviceWidth(context) * 0.45,
            decoration:  BoxDecoration(
              border: Border.all(color: Colors.grey,width: 0.4, ),
              borderRadius: BorderRadius.all( Radius.circular(15.0)),

            ),
            child:Column(
              children: [
                Expanded(
                  flex: 8,
                  child:Container(
                    width: Utility.getDeviceWidth(context) * 0.45,
                    child: Image.network('${AppConstants.BASE_URL}${categoryModel.image}',fit: BoxFit.fill,),
                  ),
                ),
                Expanded(
                    flex: 3,
                    child:Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        child: Text(categoryModel.name.toUpperCase(),style: TextStyle(fontWeight: FontWeight.w500),),
                      ),
                    )
                )
              ],
            ) // just for the demo, you can pass optionsChoices()
        ),
      ),
    );
  }
}
