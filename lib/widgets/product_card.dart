import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:medico_fast/models/medicine.dart';
import 'package:medico_fast/models/product.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/styles.dart';
import 'package:medico_fast/utils/utility.dart';




class ProductCard extends StatelessWidget{
  final Product product;
  ProductCard({  Key key, this.product,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: ()=> Navigator.of(context).pushNamed('/product_details'),
      child: Container(
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.all(10),
          height: Utility.getDeviceHeight(context) * 0.19,
          //width: Utility.getDeviceWidth(context) * 0.45,
          color: Colors.white,
          child:Row(
            children: [
              Expanded(
                  flex: 2,
                  child:Image.asset(product.imageURL)
              ),
              SizedBox(width: 10,),
              Expanded(
                  flex: 9,
                  child:Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Flexible(child: Text(product.name,style: AppStyles.blackBoldText,)),
                      SizedBox(height: 7,),
                      Text(product.title,style: AppStyles.blackText,),
                      SizedBox(height: 7,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                  children: [
                                    Text('MRP',style: AppStyles.greyText,),
                                    SizedBox(width: 5,),
                                    Text('\$8.99', style: TextStyle(decoration: TextDecoration.lineThrough,color: Colors.grey)),
                                    SizedBox(width: 10,),
                                    Container(
                                      padding: EdgeInsets.symmetric(horizontal: 8,vertical: 4),
                                      decoration: const BoxDecoration(
                                          color: AppColors.PRIMARY_COLOR,
                                        borderRadius: BorderRadius.all(Radius.circular(12))
                                      ),
                                      child:  const Center(
                                        child: const Text('10% off',style: AppStyles.whiteText,),
                                      ),
                                    ),
                                  ]
                              ),
                              Text('\$232.0',style: AppStyles.blackBoldText),
                            ],
                          ),
                          Column(
                            children: [
                              Container(
                                width: 100,
                                height: 40,
                                decoration: BoxDecoration(
                                    color: AppColors.SECONDARY_COLOR,
                                    borderRadius: BorderRadius.all(Radius.circular(6)),
                                    border: Border.all(color: AppColors.PRIMARY_COLOR)
                                ),
                                child: Center(
                                  child: Text('Wishlist',style: AppStyles.greenText,),
                                ),
                              ),
                              SizedBox(height: 10,),
                              Container(
                                width: 100,
                                height: 40,
                                decoration: BoxDecoration(
                                    color: AppColors.PRIMARY_COLOR,
                                    borderRadius: BorderRadius.all(Radius.circular(6)),
                                    border: Border.all(color: AppColors.PRIMARY_COLOR)
                                ),
                                child: Center(
                                  child: Text('Add to Cart',style: AppStyles.whiteText,),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),

                    ],
                  )
              )
            ],
          ) // just for the demo, you can pass optionsChoices()
      ),
    );
  }
}
