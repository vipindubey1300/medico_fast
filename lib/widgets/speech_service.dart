import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/styles.dart';
import 'package:medico_fast/utils/utility.dart';
import 'package:speech_to_text/speech_recognition_error.dart';
import 'package:speech_to_text/speech_recognition_result.dart';
import 'package:speech_to_text/speech_to_text.dart';


 class SpeechWidget extends StatefulWidget {
  Function(String) onSpeech;
  SpeechWidget({
    Key key,
    this.onSpeech = _onSpeech ,
  }): super(key: key);


  _SpeechWidgetState createState() => _SpeechWidgetState();

  static dynamic _onSpeech(String text) {}
}


class _SpeechWidgetState extends State<SpeechWidget>{
  bool _hasSpeech = false;
  double level = 0.0;
  double minSoundLevel = 50000;
  double maxSoundLevel = -50000;
  String lastWords = '';
  String lastError = '';
  String lastStatus = '';
  String _currentLocaleId = '';
  int resultListened = 0;
  List<LocaleName> _localeNames = [];
  final SpeechToText speech = SpeechToText();

  @override
  void initState() {
    super.initState();
    initSpeechState();
  }

  Future<void> initSpeechState() async {
    var hasSpeech = await speech.initialize(
        onError: errorListener,
        onStatus: statusListener,
        debugLogging: true,
        finalTimeout: Duration(milliseconds: 0));
    if (hasSpeech) {
      _localeNames = await speech.locales();

      var systemLocale = await speech.systemLocale();
      _currentLocaleId = systemLocale?.localeId ?? '';
    }

    if (!mounted) return;

    setState(() {
      _hasSpeech = hasSpeech;
    });
  }

  @override
  Widget build(BuildContext context) {
    return  Container(
      color: AppColors.SECONDARY_COLOR,
      height: Utility.getDeviceHeight(context) * 0.4,
      padding: EdgeInsets.all(10),
      child: Column(

        children: [
          Text('Voice Search',style: AppStyles.headerTextStyle,),
          SizedBox(height: 30,),

          SizedBox(height: 10,),
          Text(
            lastWords,
            textAlign: TextAlign.center,
            style: AppStyles.greenText,
          ),
          SizedBox(height: 20,),
          Container(
            width: 40,
            height: 40,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                    blurRadius: .26,
                    spreadRadius: level * 1.5,
                    color: Colors.black.withOpacity(.05))
              ],
              color: Colors.white,
              borderRadius:
              BorderRadius.all(Radius.circular(50)),
            ),
            child: IconButton(
              icon: Icon(Icons.mic),
              onPressed: () => null,
            ),
          ),
          SizedBox(height: 10,),
          ElevatedButton(
            onPressed: () =>  speech.isListening ? null : startListening(),
            child: Text('Start'),
            style: ElevatedButton.styleFrom(
                primary: AppColors.PRIMARY_COLOR
            ),
          ),
          SizedBox(height: 10,),
          Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            color: Theme.of(context).accentColor,
            child: Center(
              child: speech.isListening
                  ? Text(
                "I'm listening...",
                style: TextStyle(fontWeight: FontWeight.bold),
              )
                  : Text(
                'Not listening',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ),

          /**
           *  TextButton(
              onPressed: speech.isListening ? stopListening : null,
              child: Text('Stop'),
              ),
              TextButton(
              onPressed: speech.isListening ? cancelListening : null,
              child: Text('Cancel'),
              ),
           */



        ],
      ),
    );
  }

  void startListening() {
    lastWords = '';
    lastError = '';
    speech.listen(
        onResult: resultListener,
        listenFor: Duration(seconds: 30),
        pauseFor: Duration(seconds: 5),
        partialResults: true,
        localeId: _currentLocaleId,
        onSoundLevelChange: soundLevelListener,
        cancelOnError: true,
        listenMode: ListenMode.confirmation);
    setState(() {});
  }

  void stopListening() {
    speech.stop();
    setState(() {
      level = 0.0;
    });
  }

  void cancelListening() {
    speech.cancel();
    setState(() {
      level = 0.0;
    });
  }

  void resultListener(SpeechRecognitionResult result) {
    ++resultListened;
    print('Result listener $resultListened');
    widget.onSpeech(result.recognizedWords);
    setState(() {
      // lastWords = '${result.recognizedWords} - ${result.finalResult}';
      lastWords = '${result.recognizedWords} ';
    });
  }

  void soundLevelListener(double level) {
    minSoundLevel = min(minSoundLevel, level);
    maxSoundLevel = max(maxSoundLevel, level);
    // print("sound level $level: $minSoundLevel - $maxSoundLevel ");
    setState(() {
      this.level = level;
    });
  }

  void errorListener(SpeechRecognitionError error) {
    // print("Received error status: $error, listening: ${speech.isListening}");
    setState(() {
      lastError = '${error.errorMsg} - ${error.permanent}';
    });
  }

  void statusListener(String status) {
    // print(
    // 'Received listener status: $status, listening: ${speech.isListening}');
    setState(() {
      lastStatus = '$status';
    });
  }

  void _switchLang(selectedVal) {
    setState(() {
      _currentLocaleId = selectedVal;
    });
    print(selectedVal);
  }
}