import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/assets.dart';
import 'package:medico_fast/utils/styles.dart';
import 'package:medico_fast/utils/utility.dart';


class HomeAppBar extends StatelessWidget implements PreferredSizeWidget{
  HomeAppBar({@required this.text,Key key}): preferredSize = Size.fromHeight(60), super(key: key);

  final String text;
  @override
  final Size preferredSize; // default is 56.0

  @override
  Widget build (BuildContext context)  =>  AppBar(
    backgroundColor: AppColors.SECONDARY_COLOR,
    centerTitle: false,
    elevation: 0,
    automaticallyImplyLeading: false, // Don't show the leading button
    title: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        IconButton(
          onPressed: () => Scaffold.of(context).openDrawer(),
          icon: Icon(Icons.menu, color: AppColors.PRIMARY_COLOR,size: 30,),
        ),
        SizedBox(width: 10,),
        Image.asset(AssetManager.logo.value,height: 30,width: 30,),
        SizedBox(width: 10,),
        Text('Medicofast',style: AppStyles.headerTextStyle,)
        // Your widgets here
      ],
    ),
    actions: <Widget>[

      Padding(
          padding: EdgeInsets.only(right: 20.0),
          child: GestureDetector(
            onTap: () {},
            child: Icon(
                Icons.offline_pin_rounded,
                color: AppColors.PRIMARY_COLOR,
              size: 30,
            ),
          )
      ),
    ],
  );

}


