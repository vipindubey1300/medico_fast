import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:medico_fast/models/banners.dart';
import 'package:medico_fast/models/category.dart';
import 'package:medico_fast/utils/assets.dart';
import 'package:medico_fast/utils/constants.dart';


class Carousel extends StatelessWidget {

  final int type;
  final List<Banners> bannersList;

  Carousel( {
    Key key,
    @required this.type, // 1 from home page 2 from lab test
    @required this.bannersList,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) =>  CarouselSlider(
    options: CarouselOptions(
      autoPlay: true,
      enlargeCenterPage: true,
      viewportFraction: 1.9,
      aspectRatio: 2.0,
      initialPage: 1,
      height: MediaQuery.of(context).size.height * 0.23
    ),

    items: bannersList.map((i) {
      return Builder(
        builder: (BuildContext context) {
          return Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.symmetric(horizontal: 5.0),
              decoration: BoxDecoration(
                  color: Colors.transparent
              ),
             // child: Image.asset(type == 1 ? AssetManager.banner.value : AssetManager.banner2.value,fit: BoxFit.fill,)
            child: Image.network('${AppConstants.BASE_URL}${i.image}')
          );
        },
      );
    }).toList(),

  );
}