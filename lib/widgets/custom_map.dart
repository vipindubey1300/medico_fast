import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:medico_fast/utils/styles.dart';
import 'package:medico_fast/utils/utility.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:medico_fast/widgets/google_map.dart';

class CustomMapView extends StatefulWidget {
  final Function(String) onGetAddress;
  const CustomMapView({Key key, this.onGetAddress}) : super(key: key);

  @override
  _CustomMapViewState createState() => _CustomMapViewState();
}

class _CustomMapViewState extends State<CustomMapView> {
  GoogleMapController map;
  String _userAddress;

  String getAddress(){
    if(_userAddress == null) return "";
    else return _userAddress;
  }




  void setGoogleMap(double lat,double long) {
    print('Map-----${map}');
    final CameraPosition _kLake = CameraPosition(
        bearing: 192.8334901395799,
        target: LatLng(lat, long),
        tilt: 59.440717697143555,
        zoom: 19.151926040649414
    );
    map?.animateCamera(CameraUpdate.newCameraPosition(_kLake)); //means if map is not null then map.animate

  }

  //init state should not use async so we use future void function here
  Future<void> initialize() async {
    Future<Position> pos = Utility.getCurrentPosition(); //return lat long
    pos.then((value) async {
      setGoogleMap(value.latitude,value.longitude);
      String temp = await  Utility.getCurrentAddress(value);
      if(widget.onGetAddress != null) widget.onGetAddress(temp);
      if (!mounted) return; //this avoid memory leak due to setState after Screen Un-focus
      setState(() => _userAddress =  temp);

    }).catchError((onError){
      if(widget.onGetAddress != null) widget.onGetAddress('');
      //Utility().showMessage(onError.toString(),error : true);
    });

    return null;
  }


  @override
  void initState()  {
    super.initState();
    //rest code here
    initialize();

  }

  @override
  void dispose() {
    //rest code here
    map = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      clipBehavior: Clip.antiAlias,
      decoration: AppStyles.boxDecoration,
      height: Utility.getDeviceHeight(context) * 0.3,
      child:Stack(
        children: [
          Positioned(bottom: 0,left: 0,right: 0,top: 0,
              child: MapWidget(onMapCreated: (controller) =>  map = controller)
          ),


        ],
      ),
    );
  }
}