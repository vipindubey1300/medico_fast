import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:medico_fast/models/medicine.dart';
import 'package:medico_fast/models/product.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/styles.dart';
import 'package:medico_fast/utils/utility.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';




class WishlistCard extends StatelessWidget{
  final Product product;
  WishlistCard({  Key key, this.product,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: ()=> Navigator.of(context).pushNamed('/product_details'),
      child: Container(
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.all(10),
          height: Utility.getDeviceHeight(context) * 0.19,
          //width: Utility.getDeviceWidth(context) * 0.45,
          color: Colors.white,
          child:Row(
            children: [
              Expanded(
                  flex: 2,
                  child:Image.asset(product.imageURL)
              ),
              SizedBox(width: 10,),
              Expanded(
                  flex: 9,
                  child:Column(
                    //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Flexible(child: Text(product.name,style: AppStyles.blackBoldText,)),
                      SizedBox(height: 7,),

                      SizedBox(height: 7,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              RatingBar.builder(
                                initialRating: 3,
                                minRating: 1,
                                direction: Axis.horizontal,
                                allowHalfRating: true,
                                itemCount: 5,
                                itemSize: 20,
                                itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                                itemBuilder: (context, _) => Icon(
                                  Icons.star,
                                  color: Colors.amber,
                                ),
                                onRatingUpdate: (rating) {
                                  print(rating);
                                },
                              ),
                              SizedBox(height: 10,),
                              Row(
                                  children: [
                                    Text('MRP',style: AppStyles.greyText,),
                                    SizedBox(width: 5,),
                                    Text('₹8.99', style: TextStyle(decoration: TextDecoration.lineThrough,color: Colors.grey)),
                                    SizedBox(width: 10,),
                                    Container(
                                      padding: EdgeInsets.symmetric(horizontal: 8,vertical: 4),
                                      decoration: const BoxDecoration(
                                          color: AppColors.PRIMARY_COLOR,
                                          borderRadius: BorderRadius.all(Radius.circular(12))
                                      ),
                                      child:  const Center(
                                        child: const Text('10% off',style: AppStyles.whiteText,),
                                      ),
                                    ),
                                  ]
                              ),
                              SizedBox(height: 10,),
                              Text('₹232.0',style: AppStyles.blackBoldText),
                            ],
                          ),
                          Column(
                            children: [
                              Container(
                                width: 100,
                                height: 40,
                                decoration: BoxDecoration(
                                    color: Colors.red.withOpacity(0.2),
                                    borderRadius: BorderRadius.all(Radius.circular(6)),
                                    border: Border.all(color: Colors.red)
                                ),
                                child: Center(
                                  child: Text('Remove',style:TextStyle(color: Colors.red,fontWeight: FontWeight.w600),),
                                ),
                              ),
                              SizedBox(height: 10,),
                              Container(
                                width: 100,
                                height: 40,
                                decoration: BoxDecoration(
                                    color: AppColors.PRIMARY_COLOR,
                                    borderRadius: BorderRadius.all(Radius.circular(6)),
                                    border: Border.all(color: AppColors.PRIMARY_COLOR)
                                ),
                                child: Center(
                                  child: Text('Add to Cart',style: AppStyles.whiteText,),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),

                    ],
                  )
              )
            ],
          ) // just for the demo, you can pass optionsChoices()
      ),
    );
  }
}
