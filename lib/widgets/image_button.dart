import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/utility.dart';

class ImageButton extends StatelessWidget {
  final GestureTapCallback onPressed;
  final String text;
  final MaterialColor color;
  final Color iconColor;
  final IconData iconData;


  ImageButton({
    @required this.text  ,
    this.color = Colors.green,
    this.iconColor = Colors.black,
    this.iconData = Icons.add,
    this.onPressed = _onPressed //if no handler provided then call this fucntiona
  });


  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: Utility.getDeviceWidth(context) * 0.9,
      child:  ElevatedButton(
          child:Center(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(iconData,color: iconColor,),
                SizedBox(width: 15,),
                Text(text.toUpperCase(),style: TextStyle(fontSize: 16,fontFamily: "SGIcons",color: Colors.white),)
              ],
            ),
          ),
          style: ElevatedButton.styleFrom(
              primary: color,
              shadowColor: Colors.transparent,
              onPrimary:Colors.transparent,
              shape:  RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  side: BorderSide(color: Colors.transparent)
              )
          ),
          onPressed: onPressed
      ),
    );
  }

  static dynamic _onPressed() {}
}



