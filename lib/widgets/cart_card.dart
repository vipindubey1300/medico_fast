import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:medico_fast/models/medicine.dart';
import 'package:medico_fast/models/product.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/styles.dart';
import 'package:medico_fast/utils/utility.dart';


class CartCard extends StatefulWidget {
  final Product product;


  CartCard({  Key key, this.product,}) : super(key: key);

  _CartCardState createState() => _CartCardState();
}

class _CartCardState extends State<CartCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(0),
        margin: EdgeInsets.all(10),
        height: Utility.getDeviceHeight(context) * 0.17,
        //width: Utility.getDeviceWidth(context) * 0.45,

        child:Row(
          children: [
            Expanded(
                flex: 2,
                child:Image.asset(widget.product.imageURL)
            ),
            SizedBox(width: 10,),
            Expanded(
                flex: 9,
                child:Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Flexible(child: Text(widget.product.name,style: AppStyles.blackBoldText,)),
                        Icon(Icons.delete,color: Colors.red,)
                      ],
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    ),
                    SizedBox(height: 7,),
                    Text(widget.product.title.toUpperCase(),style: AppStyles.blackText,),
                    SizedBox(height: 7,),
                    Row(

                      children: [
                        Container(
                          padding: EdgeInsets.symmetric(vertical: 6,horizontal: 10),
                          decoration:  BoxDecoration(
                            border: Border.all(color: AppColors.PRIMARY_COLOR,width: 0.5, ),
                            borderRadius: BorderRadius.all( Radius.circular(8.0)),
                          ),
                          child: Center(child: Row(
                            children: [
                              Text("Quantity ",style: AppStyles.greyText,),
                              SizedBox(width: 5,),
                              Text("1",style: AppStyles.greyText)
                            ],
                          ),),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Row(
                                children: [
                                  Container(
                                    padding: EdgeInsets.symmetric(horizontal: 6,vertical: 4),
                                    decoration: const BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(6)),
                                        color: AppColors.PRIMARY_COLOR
                                    ),
                                    child:  const Center(
                                      child: const Text('10% off',style: AppStyles.whiteText,),
                                    ),
                                  ),
                                  SizedBox(width: 10,),
                                  Text('MRP',style: AppStyles.greyText,),
                                  SizedBox(width: 5,),
                                  Text('\$8.99', style: TextStyle(decoration: TextDecoration.lineThrough,color: Colors.grey))
                                ]
                            ),
                            Text('\$232.0',style: AppStyles.blackText,),
                          ],
                        )
                      ],
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    ),
                  ],
                )
            )
          ],
        ) // just for the demo, you can pass optionsChoices()
    );
  }
}
