import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:medico_fast/models/category.dart';
import 'package:medico_fast/models/medicine.dart';
import 'package:medico_fast/utils/utility.dart';


class MedicineCard extends StatefulWidget {
  final CategoryModel medicineModel;


  MedicineCard( {
        Key key,
        this.medicineModel,
      }) : super(key: key);

  _MedicineCardState createState() => _MedicineCardState();
}

class _MedicineCardState extends State<MedicineCard> {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 6,
      margin: EdgeInsets.all(10),
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Container(
          width: Utility.getDeviceWidth(context) * 0.45,
          decoration:  BoxDecoration(
            border: Border.all(color: Colors.grey,width: 0.4, ),
            borderRadius: BorderRadius.all( Radius.circular(15.0)),

          ),
          child:Column(
            children: [
              Expanded(
                flex: 8,
                child:Container(
                  width: Utility.getDeviceWidth(context) * 0.45,
                  child: Image.asset(widget.medicineModel.image,fit: BoxFit.fill,),
                ),
              ),
              Expanded(
                  flex: 3,
                  child:Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: Text(widget.medicineModel.name.toUpperCase(),style: TextStyle(fontWeight: FontWeight.w500),),
                    ),
                  )
              )
            ],
          ) // just for the demo, you can pass optionsChoices()
      ),
    );
  }
}
