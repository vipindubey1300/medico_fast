import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapWidget extends StatefulWidget {
  final MapCreatedCallback onMapCreated;
  const MapWidget({Key key, this.onMapCreated}) : super(key: key);


  @override
  _MapWidgetState createState() => _MapWidgetState();
}

class _MapWidgetState extends State<MapWidget> {
  GoogleMapController _controller;

  CameraPosition _initialCameraLocation = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );

  @override
  Widget build(BuildContext context) {
    return  GoogleMap(
      onMapCreated: (GoogleMapController controller) {
        _controller = controller;
        widget.onMapCreated(controller);
      },
      mapType: MapType.hybrid,
      myLocationEnabled: true,
      myLocationButtonEnabled: true,
      initialCameraPosition: _initialCameraLocation,
    );
  }
}