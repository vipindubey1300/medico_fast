import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:medico_fast/models/medicine.dart';
import 'package:medico_fast/models/orders.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/styles.dart';
import 'package:medico_fast/utils/utility.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';


class OrdersCard extends StatefulWidget {
  final Orders orders;


  OrdersCard({  Key key, this.orders,}) : super(key: key);

  _OrdersCardCardState createState() => _OrdersCardCardState();
}

class _OrdersCardCardState extends State<OrdersCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(0),
        margin: EdgeInsets.all(10),
        height: Utility.getDeviceHeight(context) * 0.19,
        //width: Utility.getDeviceWidth(context) * 0.45,
        color: Colors.white,
        child:Row(
          children: [
            Expanded(
                flex: 2,
                child:Image.asset(widget.orders.imageURL)
            ),
            SizedBox(width: 10,),
            Expanded(
                flex: 10,
                child:Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flexible(child: Text(widget.orders.date,style: AppStyles.blackBoldText,)),
                    Flexible(child: Text(widget.orders.name,style: AppStyles.blackText,)),
                    RatingBar.builder(
                      initialRating: 3,
                      minRating: 1,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemCount: 5,
                      itemSize: 20,
                      itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                      itemBuilder: (context, _) => Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      onRatingUpdate: (rating) {
                        print(rating);
                      },
                    ),
                    Text("Write a review",style: AppStyles.greenText,)
                  ],
                )
            ),
            SizedBox(width: 10,),
            Expanded(
                flex: 2,
                child:IconButton(
                  onPressed: (){
                    Navigator.of(context).pushNamed('/order_details');
                  },
                  icon: Icon(Icons.arrow_forward_ios_rounded,color: AppColors.PRIMARY_COLOR,),
                  splashColor: Colors.green,
                )
            ),
          ],
        ) // just for the demo, you can pass optionsChoices()
    );
  }
}
