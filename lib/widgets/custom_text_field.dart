import 'package:flutter/material.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/utility.dart';

class CustomTextField extends StatefulWidget {
  CustomTextField({
    this.label,
    this.controller,
    this.onChanged,
    this.inputType = TextInputType.text,
    this.obscureText = false,
    this.validator,
    this.enabled = true,

  });

  final String label;
  final TextEditingController controller;
  final Function validator;
  final Function onChanged;
  final TextInputType inputType;
  final bool obscureText;
  final bool enabled;

  _CustomTextFieldState createState() => _CustomTextFieldState();

}

class _CustomTextFieldState extends State<CustomTextField>{
  Color currentColor;
  FocusNode _focusNode;


  @override
  void initState() {
    super.initState();
    _focusNode = FocusNode();
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  void _onChange(String value) {
    widget.controller.text = value;
    if (widget.onChanged != null) {
      widget.onChanged(value);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Container(
        margin: EdgeInsets.only(top: 12,bottom: 12),
        width: Utility.getDeviceWidth(context) * 0.9,
        child:Theme(
          data: new ThemeData(
          // colorScheme: Colors.blue
          ),
          child:  TextField(
            focusNode: _focusNode,
            decoration: InputDecoration(
              //alignLabelWithHint: true,
              labelText: widget.label,
              fillColor: Colors.white,
              filled: true,
              border: InputBorder.none,


//          errorText: isValid ? null : invalidMsg,
//          errorStyle: TextStyle(color: Colors.white),
              labelStyle: TextStyle(color:AppColors.PRIMARY_COLOR),
              errorBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10.0),
                borderSide: BorderSide(
                  color: Colors.grey[600],
                ),
              ),
              enabledBorder:OutlineInputBorder(
                borderRadius: BorderRadius.circular(10.0),
                borderSide: BorderSide(
                  color: AppColors.PRIMARY_COLOR,
                ),
              ),
            ),
            // onChanged: (value) => _onChange,
            onChanged: (text) {
              if (widget.onChanged != null) {
                widget.onChanged(text);
              }
              // setState(() {
              //   if (!widget.validator(text) || text.length == 0) {
              //     currentColor = Colors.red;
              //   } else {
              //     currentColor = Colors.white;
              //   }
              // });
            },
            keyboardType:widget.inputType  ,
            style: TextStyle(color: Colors.black),
            controller: widget.controller,
            enabled: widget.enabled,
            obscureText: widget.obscureText,
            cursorColor: Colors.white,
            maxLines: widget.inputType == TextInputType.multiline ? 3 : 1, // in multiline just put max lines equals to null

          ),
        ),
      ),
    );
  }
}