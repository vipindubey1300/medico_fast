import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:medico_fast/models/medicine.dart';
import 'package:medico_fast/models/packages.dart';
import 'package:medico_fast/models/product.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/assets.dart';
import 'package:medico_fast/utils/styles.dart';
import 'package:medico_fast/utils/utility.dart';




class PackageCard extends StatelessWidget{
  final Packages packages;
  PackageCard({  Key key, this.packages,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: ()=> {},
      child: Container(
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.all(10),
         // height: Utility.getDeviceHeight(context) * 0.19,
          width: Utility.getDeviceWidth(context) * 0.45,

        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          border: Border.all(color: Colors.grey,width: 0.7),
          color: Colors.white,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Image.asset(AssetManager.heart.value,width: 25,height: 25,),
            Text(packages.name,style: AppStyles.blackBoldText,),
            Text('Developed By ${packages.user.name}',style: AppStyles.blackText),
            Text(' Test ${packages.name}',style: AppStyles.greyText),
            Row(
              children: [
                Text('₹${packages.price}',style: AppStyles.greenText,),
                SizedBox(width: 5,),
                Text('₹12434',style: TextStyle(color: Colors.grey,decoration: TextDecoration.lineThrough,fontSize: 12),),
                SizedBox(width: 5,),
                Text('Save ${packages.discount}%',style: AppStyles.greenText,)
              ],
            )
          ],
        ),
      ),
    );
  }
}
