import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:medico_fast/models/address.dart';
import 'package:medico_fast/models/medicine.dart';
import 'package:medico_fast/models/notification.dart';
import 'package:medico_fast/models/orders.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/styles.dart';
import 'package:medico_fast/utils/utility.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';




class NotificationCard extends StatelessWidget {

  final NotificationModel notification;
  NotificationCard({  Key key, this.notification,}) : super(key: key);





  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.all(10),
        color: Colors.white,
        child:Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    IconButton(icon: Icon(Icons.notifications), onPressed:(){},color: AppColors.PRIMARY_COLOR, ),
                    Text("Order Update",style: AppStyles.greenText,),
                  ],
                ),
                IconButton(icon: Icon(Icons.remove_circle,size: 20,), onPressed:(){},color: AppColors.PRIMARY_COLOR,)
              ],
            ),
            SizedBox(height: 10,),
            Text(notification.title,style: AppStyles.blackText),
            SizedBox(height: 15,),
            Text(notification.time,style: AppStyles.greyText),
          ],
        ) // just for the demo, you can pass optionsChoices()
    );
  }
}
