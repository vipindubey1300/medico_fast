import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/utility.dart';
import 'package:medico_fast/widgets/speech_service.dart';



class SearchBar extends StatefulWidget {
   Function onPressed;
   Function onSearch;
   Function onMicPress;



  SearchBar({
    Key key,
    this.onSearch =_onSearch ,
    this.onMicPress = _onMicPress,
    this.onPressed = _onPressed //if no handler provided then call this fucntiona
  }): super(key: key);


  _SearchBarState createState() => _SearchBarState();

   static dynamic _onPressed() {}
   static dynamic _onSearch() {}
   static dynamic _onMicPress() {}
}



class _SearchBarState extends State<SearchBar> {


  void speechModal() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: SpeechWidget(onSpeech: (text)=> searchController.text = text),
          );
        });
  }


  void onSpeechIconClick(){
    speechModal();
  }


  TextEditingController searchController = TextEditingController();


  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(20)),
      child: Container(
        height: 50,
        decoration: BoxDecoration(
          border: Border.all(color: Colors.grey,width: 1),
        ),
        margin: const EdgeInsets.all(10),
        child: Row(
          children: [
            Expanded(
              flex: 5,
              child: TextField(
                textAlignVertical: TextAlignVertical.center,
                showCursor: false,
                readOnly: true,
                onTap: () => widget.onPressed,
                controller: searchController,
                decoration: new InputDecoration(
                  filled: true,
                  fillColor: Colors.white,
                  hintText: 'Search',
                  prefixIcon: Icon(Icons.search,color:Colors.black),
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                ),
                 onSubmitted: (text)=> widget.onSearch,
                // onChanged: onSearchTextChanged,
              ),
            ),
            Expanded(
              flex: 1,
              child: GestureDetector(
                onTap: () => onSpeechIconClick(),
                child: Container(
                  color: AppColors.SECONDARY_COLOR,
                  child: new IconButton(
                      icon: new Icon(Icons.mic,color:AppColors.PRIMARY_COLOR),onPressed: null),
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }


}



