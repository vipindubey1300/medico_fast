import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/utility.dart';

class CustomButton extends StatelessWidget {
  final GestureTapCallback onPressed;
  final String text;
  final MaterialColor color;


  CustomButton({
    @required this.text  ,
    this.color = Colors.green,
    this.onPressed = _onPressed //if no handler provided then call this fucntiona
  });


  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: Utility.getDeviceWidth(context) * 0.9,
      child:  ElevatedButton(
        child: Text(text.toUpperCase(),style: TextStyle(fontSize: 16,fontFamily: "SGIcons",color: Colors.white),),
        style: ElevatedButton.styleFrom(
            primary: AppColors.PRIMARY_COLOR,
            shadowColor: Colors.transparent,
            onPrimary:Colors.transparent,
            shape:  RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
                side: BorderSide(color: Colors.transparent)
            )
        ),
        onPressed: onPressed
      ),
    );
  }

  static dynamic _onPressed() {}
}



