import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/services.dart';
import 'package:medico_fast/blocs/home/bloc.dart';
import 'package:medico_fast/blocs/login/bloc.dart';
import 'package:medico_fast/blocs/register/bloc.dart';
import 'package:medico_fast/blocs/splash/bloc.dart';
import 'package:medico_fast/screens/add_address.dart';
import 'package:medico_fast/screens/my_orders.dart';
import 'package:medico_fast/screens/order_details.dart';
import 'package:medico_fast/screens/otp.dart';
import 'package:medico_fast/screens/product_detail.dart';
import 'package:medico_fast/screens/products.dart';
import 'package:medico_fast/screens/signup.dart';
import 'package:medico_fast/screens/wishlist.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:medico_fast/blocs/api/api_bloc.dart';
import 'package:medico_fast/blocs/otp/bloc.dart';
import 'package:medico_fast/blocs/user/bloc.dart';

import 'package:medico_fast/screens/splash.dart';
import 'package:medico_fast/simple_bloc_observer.dart';




//customs
import 'blocs/blocs.dart';
import 'screens/screens.dart';

final ThemeData defaultTheme = new ThemeData(
  primaryColor: Colors.black,
  accentColor: Colors.orangeAccent,
  brightness: Brightness.light,
  visualDensity: VisualDensity.adaptivePlatformDensity,
);



void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  Bloc.observer = SimpleBlocObserver();
  //SharedPreferences.setMockInitialValues({});

  runApp(MyApp());
}




Map<String,WidgetBuilder> routes = {

  '/':  (context) => BlocProvider<SplashBloc>(
    create: (context) => SplashBloc( userBloc:  BlocProvider.of<UserBloc>(context)),
    child: SplashScreen(),
  ),
  '/login':  (context) => BlocProvider<LoginBloc>(
    create: (context) => LoginBloc(),
    child: Login(),
  ),
  '/sign_up':  (context) => BlocProvider<RegisterBloc>(
    create: (context) => RegisterBloc(),
    child: SignUp(),
  ),
  '/otp':  (context) => BlocProvider<OtpBloc>(
     create: (context) => OtpBloc( userBloc:  BlocProvider.of<UserBloc>(context)),
    child: Otp(),
  ),
  '/main_tab': (context) => MainTab(),
  '/my_orders': (context) => MyOrders(),
  '/order_details': (context) => OrderDetails(),
  '/add_address': (context) => AddAddress(),
  '/products': (context) => Products(),
  '/product_details': (context) => ProductDetail(),
  '/wishlist': (context) => WishlistPage(),
  '/home':  (context) =>MultiBlocProvider(
    providers: [
      BlocProvider<ApiBloc>(  create: (context) => ApiBloc()),
      BlocProvider<HomeBloc>(  create: (context) => HomeBloc()),
    ],
    child:new SplashScreen(),
  ),



};





class MyApp extends StatefulWidget {
  MyApp({Key key}) : super(key: key);
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp>{


  @override
  void initState(){

    super.initState();

  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider<UserBloc>(
      create:  (context) => UserBloc(),
      child: MaterialApp(
        title: 'Medical ',
        theme: defaultTheme,
        //home: Splash(),
        initialRoute: '/',
        routes: routes,
        debugShowCheckedModeBanner: false,

      ),);
  }
}






