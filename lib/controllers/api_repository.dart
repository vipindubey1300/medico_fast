import 'package:medico_fast/controllers/api_provider.dart';
import 'package:medico_fast/utils/constants.dart';

/*
We are going to use a Repository class which going to act as the inter-mediator and a layer of abstraction between the APIs and the BLOC.
 The task of the repository is to deliver movies data to the BLOC after fetching it from the API.
*/


class ApiRepository {

  ApiProvider _provider = ApiProvider();

  //to make post request , a generic method
  Future<dynamic> makePostRequest(String url, Map<String,dynamic> parameters) async {
    final response = await _provider.postRequest(parameters,url);
    return response ;
  }

  //to make post request , a generic method .. using token
  Future<dynamic> makePostRequestToken(String url, Map<String,dynamic> parameters,String token) async {
    final response = await _provider.postRequestToken(parameters,url,token);
    return response ;
  }


  //to make get request using token, a generic method
  Future<dynamic> makeGetRequestToken(String url,String token) async {
    final response = await _provider.getRequestToken(url,token);
    return response ;
  }


  //to make get request , a generic method
  Future<dynamic> makeGetRequest(String url) async {
    final response = await _provider.getRequest(url);
    return response ;
  }


  Future<dynamic> getHomeData(String url) async {
    final response = await _provider.postRequest({},url);
    return response ;
  }

  Future<dynamic> getLabData(String url) async {
    final response = await _provider.postRequest({},url);
    return response ;
  }

  Future<dynamic> postLoginData(String mobile,String deviceType,String deviceToken) async {
      Map<String, dynamic> parameters =  {
                  "mobile": mobile,
                  "device_token":deviceToken,
                  "device_type":deviceType
           };


      final response = await _provider.postRequest(parameters,AppConstants.LOGIN);
      print(response);
      return response ;
  }

  Future<dynamic> postSignUpData(String email , String password,String name,String mobile,String deviceType,String deviceToken) async {
    Map<String, dynamic> parameters =  {
      "email": email,
      "password": password,
      "mobile": mobile,
      "name":name,
      "device_token":deviceToken,
      "device_type":deviceType
    };


    final response = await _provider.postRequest(parameters,AppConstants.REGISTER);
    print(response);
    return response ;
  }


  Future<dynamic> postSocialRegister(String socialtype , String socialdata,String socialtoken,String socialid) async {

    Map<String, dynamic> parameters =  {
      "socialtype": socialtype,
      //"socialdata": socialdata,
      "socialtoken": socialtoken,
      "socialid": socialid,
    };
      print(AppConstants.SOCIAL_REGISTER);

    final response = await _provider.postRequest(parameters,AppConstants.SOCIAL_REGISTER);
    return response ;
  }



  Future<dynamic> postOtpCall(String otp,String userId ) async {
    Map<String, dynamic> parameters =  {
      "otp": otp,
      "user_id":userId
    };

    final response = await _provider.postRequest(parameters,AppConstants.VERIFY_OTP);
    print(response);
    return response ;
  }


  Future<dynamic> postForgotPassword(String email ) async {
    Map<String, dynamic> parameters =  {
      "email": email,
    };

    final response = await _provider.postRequest(parameters,AppConstants.VERIFY_OTP);
    print(response);
    return response ;
  }

  Future<dynamic> postResetPassword(String email ,String password, String confirmPassword) async {
    Map<String, dynamic> parameters =  {
      "email": email,
      "password":password,
      "password_confirmation":confirmPassword
    };

    final response = await _provider.postRequest(parameters,AppConstants.RESET_PASSWORD);
    print(response);
    return response ;
  }


}