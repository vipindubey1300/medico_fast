import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:medico_fast/blocs/home/bloc.dart';
import 'package:medico_fast/blocs/home/event.dart';
import 'package:medico_fast/blocs/home/home_bloc.dart';
import 'package:medico_fast/models/category.dart';
import 'package:medico_fast/models/medicine.dart';
import 'package:medico_fast/screens/navigation_drawer.dart';
import 'package:medico_fast/screens/products.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/assets.dart';
import 'package:medico_fast/utils/styles.dart';
import 'package:medico_fast/utils/utility.dart';
import 'package:medico_fast/widgets/category_card.dart';
import 'package:medico_fast/widgets/home_header.dart';
import 'package:medico_fast/widgets/carousel.dart';
import 'package:medico_fast/widgets/medicine_card.dart';
import 'package:medico_fast/widgets/progress_bar.dart';
import 'package:medico_fast/widgets/search_bar.dart';
import 'package:medico_fast/widgets/speech_service.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {



  var address = '';

  HomeBloc _homeBloc;


  //init state should not use async so we use future void function here
  Future<void> initialize() async {
    Future<Position> pos = Utility.getCurrentPosition(); //return lat long
    pos.then((value) async {
      String temp = await  Utility.getCurrentAddress(value);
      setState(() => address =  temp);

    }).catchError((onError){
      print(onError.toString());
    });

    return null;
  }


  @override
  void initState()  {
    super.initState();
    //rest code here
    initialize();
    _homeBloc = BlocProvider.of<HomeBloc>(context);
    _homeBloc.add(GetHomeData());

  }

  @override
  void dispose() {
    _homeBloc.close();
    super.dispose();

  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.SECONDARY_COLOR,
      appBar: HomeAppBar(),
      drawer: NavDrawer(),
      body:  BlocListener<HomeBloc, HomeState>(
        listener: (context, state) {
          if (state is HomeError) {
            Utility.showMessage(state.error,error: true);
          }
        },
        child:Stack(
          fit: StackFit.expand,
          children: [
            _buildBody(),
            _buildLoader()
          ],
        )
      ),

    );
  }

  Widget _buildBody() => SingleChildScrollView(
    child: Column(

      children: [
        SearchBar(),

        //location view
        Container(
          height: 50,
          color: AppColors.PRIMARY_COLOR,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(Icons.location_on,color: Colors.white,),
                  SizedBox(width: 10,),
                  if(address.length > 0 ) Flexible(child: Text(address.substring(0,40),style:AppStyles.whiteText,overflow: TextOverflow.fade, softWrap: false,),) else Text('No Address Found',style:AppStyles.errorBigText,)

                ],
              ),
              Text('Change'.toUpperCase(),style:AppStyles.whiteBoldText)
            ],
          ),
        ),
        BlocBuilder<HomeBloc, HomeState>(
          builder: (context, state) {
            if (state is HomeData) return  Carousel(type: 1,bannersList: state.bannersList,);
            //else if (state is HomeLoading)  return  ProgressBar();
            return Container();
          },
        ),

        new Container(
          // height: 60,
          padding: EdgeInsets.symmetric(vertical: 20,horizontal: 10),
          decoration: BoxDecoration(
            color: AppColors.SECONDARY_COLOR,
            boxShadow: [
              BoxShadow(
                blurRadius: 3.0,
                offset: Offset(0.0, 0.9),
              ),
            ],
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Icon(Icons.ac_unit_sharp),
                  SizedBox(width: 10,),
                  Text("Categories",style: AppStyles.blackMediumText,)
                ],
              ),
              Text("VIEW ALL",style: AppStyles.blackText,)
            ],
          ),
        ),
        BlocBuilder<HomeBloc, HomeState>(
          builder: (context, state) {
            if (state is HomeData) return Container(
                height: 230,
                padding: EdgeInsets.symmetric(vertical: 10),
                child: ListView.builder(
                  itemCount: state.categoryList.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, int i) => CategoryCard(categoryModel: state.categoryList[i],),
                )
            );
            //else if (state is HomeLoading)  return  ProgressBar();
            return Container();
          },
        ),

        new Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: AppColors.SECONDARY_COLOR,
              boxShadow: [
                BoxShadow(
                  blurRadius: 3.0,
                  offset: Offset(5.0, -0.29),
                ),
              ],
            ),
            child: Row(
              children: [
                Expanded(
                  flex: 6,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Order Medicine",style: AppStyles.blackMediumText,),
                      SizedBox(height: 10,),
                      Text("Upload Prescription and tell us what do you need ?",style: AppStyles.greyText,),
                      Text("We do the rest",style: AppStyles.greyText,),
                      SizedBox(height: 15,),
                      Text("Save upto 60% off",style: AppStyles.blackBoldText,),
                      SizedBox(height: 20,),
                      ElevatedButton(
                        onPressed: (){},
                        child: Text('Order Now'),
                        style: ElevatedButton.styleFrom(
                            elevation: 2,
                            primary: AppColors.PRIMARY_COLOR,
                            padding: EdgeInsets.symmetric(horizontal: 40, vertical: 13),
                            textStyle: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold)
                        ),)
                    ],
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Image.asset(AssetManager.bottle.value),
                )
              ],
            )
        ),



      ],
    ),
  );

  Widget _buildLoader() =>  BlocBuilder<HomeBloc, HomeState>(
    builder: (context, state) {
       if (state is HomeLoading)  return  ProgressBar();
       return Container();
    },
  );
}