import 'package:flutter/material.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/assets.dart';
import 'package:medico_fast/utils/styles.dart';


class NavDrawer extends StatefulWidget {
  @override
  _NavDrawState createState() => _NavDrawState();
}

int selectedIndex = 0;

class _NavDrawState extends State<NavDrawer> {


  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        padding: EdgeInsets.all(10),
        child: ListView(
          children: <Widget>[
            SizedBox(height: 10,),
            Row(
              children: [
                Image.asset(AssetManager.logo.value,width: 60,height: 60,),
                SizedBox(width: 10,),
                Text('Medicofast',style: AppStyles.appNameStyle,)
              ],
            ),
            SizedBox(height: 100,),
            _createDrawerItem(
                icon: Icons.privacy_tip_outlined,
                text: 'Privacy Policy',
            ),
            _createDrawerItem(
              icon: Icons.reset_tv,
              text: 'Return Policy',
            ),
            _createDrawerItem(
              icon: Icons.help,
              text: 'Help Center',
            ),


          ],
        ),
      ),
    );
  }
}

Widget _createDrawerItem({IconData icon, String text, GestureTapCallback onTap}) {
  return Container(
    color: AppColors.SECONDARY_COLOR,
    padding: EdgeInsets.only(top:10,left:10,right:10,bottom:10),
    margin:  EdgeInsets.only(top:4,left:10,right:10,bottom:4),
    child: Ink(
      child: ListTile(
        selected: true,
        hoverColor: Colors.white,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
           Icon(icon),
            SizedBox(width: 10,),
            Text(text,style: AppStyles.blackText,)
          ],
        ),
        onTap: onTap,
      ),
    ),
  );
}




