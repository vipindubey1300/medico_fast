import 'package:flutter/material.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/assets.dart';
import 'package:medico_fast/utils/styles.dart';
import 'package:medico_fast/utils/utility.dart';
import 'package:medico_fast/widgets/back_header.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:medico_fast/widgets/bottom_view.dart';


class ProductDetail extends StatefulWidget {
  @override
  _ProductDetailState createState() => _ProductDetailState();
}

class _ProductDetailState extends State<ProductDetail> {

  Widget deliveryWidget() => Container(
    color: Colors.white,
    padding: EdgeInsets.all(10),
    child: Column(
      children: [
        Row(
          children: [
            Icon(Icons.location_on),
            SizedBox(width: 10,),
            Text('Product Delivery Available at :',style: AppStyles.blackBoldText,)
          ],
        ),
        SizedBox(height: 10,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('D-12 Lalita Park Laxminagar Delhi',style: AppStyles.blackText,),
            SizedBox(width: 10,),
            Text('CHANGE',style: AppStyles.greenTextBold,)
          ],
        ),
      ],
    ),
  );
  Widget detailsWidget() => Container(
    padding: EdgeInsets.all(10),
    decoration: BoxDecoration(
        border: Border.all(color:AppColors.PRIMARY_COLOR),
        borderRadius: BorderRadius.all(Radius.circular(10))
    ),
    child: Column(
      //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Flexible(child: Text('Accu - Blood Pressure Check',
            style:TextStyle(fontSize: 17,fontWeight: FontWeight.w600))),
        SizedBox(height: 7,),

        SizedBox(height: 7,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                RatingBar.builder(
                  initialRating: 3,
                  minRating: 1,
                  direction: Axis.horizontal,
                  allowHalfRating: true,
                  itemCount: 5,
                  itemSize: 30,
                  itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                  itemBuilder: (context, _) => Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  onRatingUpdate: (rating) {
                    print(rating);
                  },
                ),
                SizedBox(height: 10,),
                Row(
                    children: [
                      Text('MRP',style: AppStyles.greyText,),
                      SizedBox(width: 5,),
                      Text('₹8.99', style: TextStyle(decoration: TextDecoration.lineThrough,color: Colors.grey)),
                      SizedBox(width: 10,),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 8,vertical: 4),
                        decoration: const BoxDecoration(
                            color: AppColors.PRIMARY_COLOR,
                            borderRadius: BorderRadius.all(Radius.circular(12))
                        ),
                        child:  const Center(
                          child: const Text('10% off',style: AppStyles.whiteText,),
                        ),
                      ),
                    ]
                ),
                SizedBox(height: 10,),
                Text('₹232.0',style: AppStyles.greenTextBold),
              ],
            ),
            Column(
              children: [
                Container(
                  width: 100,
                  height: 40,
                  decoration: BoxDecoration(
                      color: AppColors.SECONDARY_COLOR,
                      borderRadius: BorderRadius.all(Radius.circular(6)),
                      border: Border.all(color: AppColors.PRIMARY_COLOR)
                  ),
                  child: Center(
                    child: Text('Wishlist',style: AppStyles.greenText,),
                  ),
                ),
                SizedBox(height: 10,),
                Container(
                  width: 100,
                  height: 40,
                  decoration: BoxDecoration(
                      color: AppColors.PRIMARY_COLOR,
                      borderRadius: BorderRadius.all(Radius.circular(6)),
                      border: Border.all(color: AppColors.PRIMARY_COLOR)
                  ),
                  child: Center(
                    child: Text('Add to Cart',style: AppStyles.whiteText,),
                  ),
                ),
              ],
            )
          ],
        ),

      ],
    ),
  );

  Widget bullet() => Container(
    height: 10.0,
    width: 10.0,
    decoration: new BoxDecoration(
      color: Colors.black,
      shape: BoxShape.circle,
    ),
  );




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BackHeader(text: 'Product Page',),
      backgroundColor: AppColors.SECONDARY_COLOR,
      body: SafeArea(
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.only(bottom: 50),
              child: SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      deliveryWidget(),
                      SizedBox(height: 15,),
                      Container(
                        height: Utility.getDeviceHeight(context) * 0.33,
                        width:  Utility.getDeviceWidth(context),
                        color: Colors.white,
                        child: Image.asset(AssetManager.product.value),
                      ),
                      SizedBox(height: 20,),
                      detailsWidget(),
                      SizedBox(height: 15,),
                      Container(
                        color: Colors.white,
                        padding: EdgeInsets.all(10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Description",style: AppStyles.blackBoldText,),
                            SizedBox(height: 10,),
                            Text("What is Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry's standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has?",
                              style: AppStyles.blackText,),
                            SizedBox(height: 25,),
                            Text("Specifications And Features",style: AppStyles.blackBoldText,),
                            SizedBox(height: 10,),
                            Row(
                              children: [
                                bullet(),
                                SizedBox(width: 10,),
                                Flexible(child: Text("This products relieve the weight and losses the height and the")),
                              ],
                            ),
                            SizedBox(height: 7,),
                            Row(
                              children: [
                                bullet(),
                                SizedBox(width: 10,),
                                Flexible(child: Text("This products relieve the weight and losses the height and the")),
                              ],
                            ),
                            SizedBox(height: 7,),
                            Row(
                              children: [
                                bullet(),
                                SizedBox(width: 10,),
                                Flexible(child: Text("This products relieve the weight and losses the height and the")),
                              ],
                            ),
                            SizedBox(height: 7,),
                            Row(
                              children: [
                                bullet(),
                                SizedBox(width: 10,),
                                Flexible(child: Text("This products relieve the weight and losses the height and the")),
                              ],
                            )
                          ],
                        ),
                      )
                    ],

                  ),
                ),
              ),
            ),
            BottomView(
              leftIcon: Icons.shopping_bag,
              leftText: 'Add to Wishlist',
              rightIcon: Icons.shopping_cart,
              rightText: 'Add to Cart',
            )
          ],
        ),
      ),
    );
  }
}