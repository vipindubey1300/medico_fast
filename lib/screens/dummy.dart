import 'package:flutter/material.dart';
import 'package:medico_fast/utils/app_colors.dart';


class Dummy extends StatefulWidget {
  @override
  _DummyState createState() => _DummyState();
}

class _DummyState extends State<Dummy> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.SECONDARY_COLOR,
      body: Container(),
    );
  }
}