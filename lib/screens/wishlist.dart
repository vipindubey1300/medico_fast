import 'package:flutter/material.dart';
import 'package:medico_fast/models/product.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/assets.dart';
import 'package:medico_fast/widgets/back_header.dart';
import 'package:medico_fast/widgets/product_card.dart';
import 'package:medico_fast/widgets/wishlist_card.dart';



class WishlistPage extends StatefulWidget {
  @override
  _WishlistPageState createState() => _WishlistPageState();
}

class _WishlistPageState extends State<WishlistPage> {
  List<Product> products = [
    Product(id: '1',name:'Accu-Check Glumotere Test Strip (Pack of 50)',imageURL: AssetManager.product.value,price: '100',discount: '10',title: 'By general Chemist'),
    Product(id: '2',name:'Ayurvedic',imageURL: AssetManager.product.value,price: '100',discount: '10',title: 'By general Chemist'),
    Product(id: '3',name:'Lab Test',imageURL: AssetManager.product.value,price: '100',discount: '10',title: 'By general Chemist'),
  ];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.SECONDARY_COLOR,
      appBar: BackHeader(text: 'My Wishlist',),
      body: Column(
        children: [
          Container(
            //color: Colors.white,
            margin: EdgeInsets.all(10),
            child:  ListView.separated(
              separatorBuilder: (BuildContext context, int index) => Divider(height: 1),
              itemCount: products.length,
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              scrollDirection: Axis.vertical,
              itemBuilder: (context, int i) => WishlistCard(product: products[i],),

            ),

          ),
        ],
      ),
    );
  }
}
