import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:medico_fast/blocs/login/bloc.dart';
import 'package:medico_fast/blocs/user/bloc.dart';
import 'package:medico_fast/blocs/user/event.dart';
import 'package:medico_fast/blocs/user/state.dart';
import 'package:medico_fast/screens/address.dart';
import 'package:medico_fast/screens/login.dart';
import 'package:medico_fast/screens/my_orders.dart';
import 'package:medico_fast/screens/wishlist.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/assets.dart';
import 'package:medico_fast/utils/storage_helper.dart';
import 'package:medico_fast/utils/styles.dart';
import 'package:medico_fast/utils/utility.dart';
import 'package:medico_fast/widgets/image_button.dart';
import 'package:medico_fast/widgets/image_header.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {

  UserBloc _userBloc;

  @override
  void initState() {
    _userBloc =  BlocProvider.of<UserBloc>(context);
    super.initState();

  }

  @override
  void dispose() {
    super.dispose();

  }


  void _onLogout(){
    MySharedPreferences.instance.removeAll();
    _userBloc.add(UserLoggedOut());
    Navigator.pushNamedAndRemoveUntil(context, '/login', (route) => false);
    // Navigator.pushAndRemoveUntil(
    //     context,
    //     MaterialPageRoute(builder: (BuildContext context) =>  BlocProvider<LoginBloc>(
    //       create: (context) => LoginBloc(),
    //       child: Login(),
    //     )),
    //     ModalRoute.withName('/')
    // );

  }

  Widget buildImageContainer() =>  Container(
    height: Utility.getDeviceHeight(context) * 0.35,
    color: Colors.white,
    child: Stack(
      children: [
        Image.asset(AssetManager.martha.value,fit: BoxFit.fill,
            width: Utility.getDeviceWidth(context)),
        Positioned(
          right: 25,
          bottom: 25,
          child: ClipRect(
            child: BackdropFilter(
              filter: new ImageFilter.blur(sigmaX: 1.0, sigmaY: 1.0),
              child: new Container(
                width: 60.0,
                height: 60.0,
                decoration: new BoxDecoration(
                    color: Colors.grey.shade200.withOpacity(0.5),
                    borderRadius: BorderRadius.all(Radius.circular(8))
                ),
                child: new Center(
                  child: new Icon(Icons.camera_alt,size: 25,),
                ),
              ),
            ),
          ),
        ),

      ],
    ),
  );


  Widget userDetails () => Container(
    child: BlocBuilder<UserBloc, UserState>(
      builder: (context, state) {
        if(state.user != null) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('${state.user.name}',style: AppStyles.whiteMediumText,),
              Row(
                children: [
                  Text("Name : ",style: TextStyle(color: Colors.black.withOpacity(0.5))),
                  SizedBox(width: 6,),
                  Text('${state.user.name}',style: AppStyles.whiteText),
                ],
              ),
              Row(
                children: [
                  Text("Email : ",style:  TextStyle(color: Colors.black.withOpacity(0.5))),
                  SizedBox(width: 6,),
                  Text('${state.user.email}',style: AppStyles.whiteText),
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Text("Mobile  : ",style: TextStyle(color: Colors.black.withOpacity(0.5))),
                      SizedBox(width: 6,),
                      Text('${state.user.token}',style: AppStyles.whiteText),
                    ],
                  ),
                  ClipRect(
                    child: BackdropFilter(
                      filter: new ImageFilter.blur(sigmaX: 1.0, sigmaY: 1.0),
                      child: new Container(
                        width: 60.0,
                        height: 60.0,
                        decoration: new BoxDecoration(
                            color: Colors.grey.shade200.withOpacity(0.5),
                            borderRadius: BorderRadius.all(Radius.circular(8))
                        ),
                        child: new Center(
                          child: new Icon(Icons.edit,size: 25,color: AppColors.PRIMARY_COLOR,),
                        ),
                      ),
                    ),
                  )
                ],
              )
            ],
          );
        }
        else return Container();
      },
    ),
  );

  Widget buildAccountContainer() =>  Container(
    height: Utility.getDeviceHeight(context) * 0.26,
    width: Utility.getDeviceWidth(context),
    color: AppColors.PRIMARY_COLOR,
    padding: EdgeInsets.all(10),
    child: userDetails(),
  );



  Widget buildButton({IconData icon, String text, GestureTapCallback onTap}) => Container(
    color: Colors.white,
    padding: EdgeInsets.only(top:15,left:10,right:10,bottom:15),
    margin: EdgeInsets.all(5),
    child: GestureDetector(
      onTap:  onTap,
      child: Row(
        children: [

          Icon(icon),
          SizedBox(width: 10,),
          Text(text,style: AppStyles.blackText,)
        ],
      ),
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.SECONDARY_COLOR,
      appBar: ImageAppBar(text: 'Profile',icon: Icons.account_circle,),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Column(
            children: [
              buildImageContainer(),
              const SizedBox(height: 15,),
              buildAccountContainer(),
              const SizedBox(height: 15,),
              Row(
                children: [
                 Expanded(child:  buildButton(icon: Icons.wallet_giftcard,text: 'My Wishlist',onTap: _navigateToWishlist),flex: 1,),
                  Expanded(child:  buildButton(icon: Icons.payment,text: 'Payment Method'),flex: 1,),
                ],
              ),
              const SizedBox(height: 8,),
              Row(
                children: [
                  Expanded(child:  buildButton(icon: Icons.shopping_basket,text: 'My Orders',onTap: _navigateToOrders),flex: 1,),
                  Expanded(child:  buildButton(icon: Icons.home,text: 'Address',onTap: _navigateToAddress),flex: 1,),
                ],
              ),
              const SizedBox(height: 15,),
              ImageButton(text: 'Sign out',color: Colors.red,iconColor: Colors.white,iconData: Icons.login,onPressed: () => _onLogout(),),
              SizedBox(height: 20,)

            ],
          ),
        ),
      ),
    );
  }

  void _navigateToOrders() {
   // Navigator.of(context).pushNamed('/my_orders');
   //  Navigator.push(context, new MaterialPageRoute(
   //      builder: (context) => new MyOrders())
   //  );
    Navigator.of(context, rootNavigator: true)
        .push(MaterialPageRoute(builder: (context) => new MyOrders()));

  }

  void _navigateToAddress() {
    Navigator.of(context, rootNavigator: true)
        .push(MaterialPageRoute(builder: (context) => new Addresses()));
  }

  void _navigateToWishlist() {
    Navigator.of(context, rootNavigator: true)
        .push(MaterialPageRoute(builder: (context) => new WishlistPage()));
  }
}