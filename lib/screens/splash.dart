import 'dart:async';

import 'package:medico_fast/blocs/splash/bloc.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/assets.dart';
import 'package:flutter/material.dart';
import 'package:medico_fast/utils/utility.dart';
import '../utils/storage_helper.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:medico_fast/blocs/splash/splash_bloc.dart';


class SplashScreen extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<SplashScreen> {

  SplashBloc _splashBloc;

  @override
  void initState()  {
    super.initState();
    //rest code here
    _splashBloc = BlocProvider.of<SplashBloc>(context);
    startTime();

  }

  @override
  void dispose() {
    //rest code here
    _splashBloc.close();
    super.dispose();
  }

  void navigationPage() async{
    String id = await MySharedPreferences.instance.getStringValue('id');
    print('id is ' + id.toString());
    if(id.isEmpty) Navigator.pushReplacementNamed(context, '/login');
    else {
      _splashBloc.add(
        SplashSaveUser(
          email: 'abc@gmail.com',
          name: 'vip',
          id: id,
          token: 'dq23d'
        )
      );
      Navigator.pushReplacementNamed(context, '/main_tab');
    }
  }


  startTime() async {
    var _duration = new Duration(seconds: 2);
    return new Timer(_duration, navigationPage);
  }


  Widget logoPhoto() => Image.asset(
    AssetManager.logo.value,
    width: MediaQuery.of(context).size.width * 0.6,
    fit: BoxFit.contain,
    height: Utility.getDeviceHeight(context) * 0.4,
  );


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColor.black,
        body: Container(
            decoration:BoxDecoration(
              image:DecorationImage(
                  fit:BoxFit.fill,
                  image: AssetImage(AssetManager.backgroundImage.value),
                   )
                ),
            child: Center(
              child:logoPhoto() ,
            ),
        )
    );
    ;
  }
}

/**
 *  List.generate(6, (index) {
    return Center(
    child: RaisedButton(
    onPressed: (){},
    color: Colors.greenAccent,
    child: Text(
    '$index AM',
    ),
    ),
    );
    })
 */
