import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:medico_fast/blocs/login/bloc.dart';
import 'package:medico_fast/blocs/login/event.dart';
import 'package:medico_fast/blocs/login/login_bloc.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/assets.dart';
import 'package:medico_fast/utils/strings.dart';
import 'package:medico_fast/utils/styles.dart';
import 'package:medico_fast/utils/utility.dart';
import 'package:medico_fast/utils/validators.dart';
import 'package:medico_fast/widgets/custom_button.dart';
import 'package:medico_fast/widgets/custom_text_field.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'dart:io' show Platform;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:medico_fast/widgets/progress_bar.dart';
import 'package:medico_fast/widgets/touchable_widget.dart';

class Login extends StatefulWidget {
  Login({Key key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login>{


  bool isChecked = false;
  String deviceToken = "gvyc";
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  LoginBloc _loginBloc;


  @override
  void initState()  {
    super.initState();
    //rest code here
    _loginBloc = BlocProvider.of<LoginBloc>(context);

  }

  @override
  void dispose() {
    //rest code here
    _loginBloc.close();
    super.dispose();
  }

  TextEditingController _phoneController = TextEditingController();


  _onLoginButtonPressed() {
    String phone = _phoneController.text;
    //dismiss keyboard
    FocusScope.of(context).requestFocus(FocusNode());
    if(!Validators.validateField(phone)) Utility.showMessage('Enter Phone Number');
    else if(!Validators.validatePhone(phone)) Utility.showMessage('Phone Number Must Be Between 10 Digits');
    else  _loginBloc.add(LoginSubmit(mobile: phone,deviceToken: deviceToken , deviceType: Platform.isIOS ? '2' : '1'));



  }


  @override
  Widget build(BuildContext context) {
    // print(_loginBloc.state.toString());
    return  Scaffold(
        key: _scaffoldKey,
        body:  BlocListener<LoginBloc, LoginState>(
          listener: (context, state) {
            if (state is LoginError) {
              Utility.showMessage(state.error,error: true);
            }
            if (state is LoginSuccess){
              var response = state.jsonResponse;
              var user = response['user'];
              Navigator.pushNamed(context,'/otp',arguments: {
                'route': 'login',
                'id':user['_id'],
                'name':user['name'],
                'email':user['email'],
                'token':user['unique_token']
              });
            }
          },
          child: _authForm(),
        )

    );
  }

  Widget _authForm() => Stack(
    fit: StackFit.expand,
    children: [
      Container(
        decoration: new BoxDecoration(
            image: new DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage(AssetManager.backgroundImage.value),
            )),
      ),
      _loginForm(),
      BlocBuilder<LoginBloc, LoginState>(
        builder: (context, state) {
          if (state is LoginLoading)  return  ProgressBar();
          return Container();
        },
      )
    ],
  );


  Widget getSocialButton(String imageName) =>Align(
    alignment: Alignment.center,
    child:  InkWell(
      child: Container(
        height: Utility.getDeviceHeight(context) * 0.11,
        width:  Utility.getDeviceHeight(context) * 0.11,
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: AppColors.SECONDARY_COLOR,
          borderRadius: BorderRadius.circular(6),
          border: Border.all(
            color: Colors.grey, //                   <--- border color
            width: 1.0,
          ),
        ),
        child: Center(
          child: Image.asset(imageName,height: 30,width: 30,),
        ),
      ),
    ),
  );

  Widget _loginForm() => SafeArea(
    child: Center(
      child:  SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(height: 30,),
            Image.asset(
              AssetManager.logo.value,
              width: MediaQuery.of(context).size.width * 0.37,
              fit: BoxFit.fill,
              height: Utility.getDeviceHeight(context) * 0.18,
            ),
            const SizedBox(height: 15,),
            const Text(Strings.appName,style: AppStyles.appNameStyle,),
            const SizedBox(height: 15,),
            Image.asset(
              AssetManager.hospital.value,
              width: MediaQuery.of(context).size.width * 0.7,
              fit: BoxFit.fill,
              height: Utility.getDeviceHeight(context) * 0.21,
            ),
            const SizedBox(height: 0,),
             Container(
              width: Utility.getDeviceWidth(context) * 0.7,
              height: 60,
              decoration: AppStyles.boxDecoration,
               padding: EdgeInsets.symmetric(horizontal: 10),
               child: Row(
                 mainAxisSize: MainAxisSize.max,
                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                 children: [
                   Text('${Strings.alreadyHaveAccount}?',style: AppStyles.smallTextStyle,),
                   TouchableWidget(
                     onTap: () => Navigator.of(context).pushNamed('/sign_up'),
                     child: Container(
                       padding: EdgeInsets.symmetric(horizontal: 5,vertical: 3),
                       height: 35,
                       width: 80,
                       decoration:BoxDecoration(
                         color: AppColors.SECONDARY_COLOR,
                         borderRadius: BorderRadius.circular(6),
                         border: Border.all(
                           color: AppColors.PRIMARY_COLOR, //                   <--- border color
                           width: 1.9,
                         ),
                       ),
                       child: Center(
                         child: Text(Strings.signup,style: TextStyle(color: AppColors.PRIMARY_COLOR,fontWeight: FontWeight.w600),),
                       ),
                     ),
                   )
                 ],
               ),
            ),
            const SizedBox(height: 20,),
            CustomTextField(
              label: 'Phone Number',
              inputType: TextInputType.phone,
              controller: _phoneController,
            ),

            Align(
              alignment: Alignment.centerRight,
              child:  GestureDetector(
                onTap: ()=>  Navigator.pushNamed(context,'/forgot_password'),
                child: Text("Forgot Password ?     ",style: TextStyle(color:Colors.white,fontSize: 18),),
              ),
            ),
            SizedBox(height: 10,),
            CustomButton(text: 'Sign In',onPressed: ()=> {
              // Navigator.pushNamed(context,'/home')
              _onLoginButtonPressed()
            },),
            SizedBox(height: 15,),
            Text('${Strings.otherSignInOptions}',style: AppStyles.mediumTextStyle,),
            SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround  ,
              children: [
                Expanded(
                  flex:1,
                    child: getSocialButton(AssetManager.google.value)
                ),
                Expanded(
                    flex:1,
                    child: getSocialButton(AssetManager.fb.value)
                ),
                Expanded(
                    flex:1,
                    child: getSocialButton(AssetManager.mail.value)
                )
              ],
            )
          ],
        ),
      ),
    ),
  );



}
