import 'package:flutter/material.dart';
import 'package:medico_fast/models/address.dart';
import 'package:medico_fast/models/orders.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/assets.dart';
import 'package:medico_fast/utils/utility.dart';
import 'package:medico_fast/widgets/address_card.dart';
import 'package:medico_fast/widgets/back_header.dart';
import 'package:medico_fast/widgets/image_button.dart';
import 'package:medico_fast/widgets/orders_card.dart';



class Addresses extends StatefulWidget {
  @override
  _AddressesState createState() => _AddressesState();
}

class _AddressesState extends State<Addresses> {
  List<Address> addresses = [
   Address(id: '1',phone: '2133213232',address: 'New delhi india universe 110093'),
   Address(id: '1',phone: '2133213232',address: 'New delhi india universe 110093'),
  ];



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.SECONDARY_COLOR,
      appBar: BackHeader(text: 'Address',),
      body: Column(
        children: [
          Container(
            //color: Colors.white,
            margin: EdgeInsets.all(10),
            child:  ListView.separated(
              separatorBuilder: (BuildContext context, int index) => Divider(height: 1),
              itemCount: addresses.length,
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              scrollDirection: Axis.vertical,
              itemBuilder: (context, int i) => AddressCard(address: addresses[i],),

            ),

          ),
          ImageButton(
            text: 'Add New Address',
            onPressed: () => Navigator.of(context).pushNamed('/add_address'),
            iconColor: Colors.white,
            iconData: Icons.home_outlined,
          )
        ],
      ),
    );
  }
}
