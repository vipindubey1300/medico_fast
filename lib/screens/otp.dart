import 'package:flutter/material.dart';
import 'package:medico_fast/blocs/otp/bloc.dart';
import 'package:medico_fast/blocs/otp/event.dart';
import 'package:medico_fast/blocs/otp/state.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/assets.dart';
import 'package:medico_fast/utils/styles.dart';
import 'package:medico_fast/utils/utility.dart';
import 'package:medico_fast/utils/validators.dart';
import 'package:medico_fast/widgets/back_header.dart';
import 'package:medico_fast/widgets/custom_text_field.dart';
import 'package:medico_fast/widgets/custom_button.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:medico_fast/widgets/progress_bar.dart';


class Otp extends StatefulWidget {
  @override
  _OtpState createState() => _OtpState();
}

class _OtpState extends State<Otp> {


  OtpBloc _otpBloc;
  String id;
  String name;
  String email;
  String token;

  @override
  void initState()  {
    super.initState();
    _otpBloc = BlocProvider.of<OtpBloc>(context);



    //getting data from previous route
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final Map arguments = ModalRoute.of(context).settings.arguments as Map;
      if (arguments != null){
        //means came from register screen
        print('${arguments['route'] }  route');
        //setState(() => _isFirstTime = true);
      //  _dialogBloc.add(true);

         id = arguments['id'];
        name = arguments['name'];
        email = arguments['email'];
        token = arguments['token'];

      }
      else print(' No Previous Route Found');
    });
  }

  @override
  void dispose() {
    //rest code here
    _otpBloc.close();
    super.dispose();
  }
  TextEditingController _otpController = TextEditingController();


  _onVerifyButtonPressed() {
    String otp = _otpController.text;
    //dismiss keyboard
    FocusScope.of(context).requestFocus(FocusNode());
    if(!Validators.validateOtp(otp)) Utility.showMessage('Enter Valid Otp');
    else  _otpBloc.add(
          OtpSubmit(otp: otp,userId: id)
       );

  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BackHeader(text: 'OTP',),
      body: BlocListener<OtpBloc, OtpState>(
          listener: (context, state) {
            if (state is OtpError) {
              Utility.showMessage(state.error,error: true);
              //Navigator.pushNamed(context,'/home');
            }
            if (state is OtpSuccess){
              _otpBloc.add(OtpSaveUser(id: id, name: name, email: email, token: token));
              Navigator.of(context).pushNamed('/main_tab');
            }
          },
          child: _otpForm(),
        )
    );
  }

  Widget _otpForm() => Stack(
    fit: StackFit.expand,
    children: [
      Container(
        decoration: new BoxDecoration(
            image: new DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage(AssetManager.backgroundImage.value),
            )),
      ),
     _body(),
      BlocBuilder<OtpBloc, OtpState>(
        builder: (context, state) {
          if (state is OtpLoading)  return  ProgressBar();
          return Container();
        },
      )

    ],
  );

  Widget _body() =>   SingleChildScrollView(
    child: Container(
      padding: EdgeInsets.all(20),
      child: Center(
        child:  Column(
          children: [
            Image.asset(AssetManager.phoneView.value),
            SizedBox(height: 30,),
            Container(
              decoration: AppStyles.boxDecoration,
              padding: EdgeInsets.all(17),
              child: Center(
                child: Text("An SMS message containing an One Time Password (OTP) has been sent to mobile number +91 - XXXX XXXX 78 ",style: AppStyles.smallTextStyle,),
              ),
            ),
            SizedBox(height: 20,),
            CustomTextField(
              label: 'OTP',
              inputType: TextInputType.phone,
              controller: _otpController,
            ),
            SizedBox(height: 20,),
            CustomButton(text: 'Verify',onPressed: ()=> _onVerifyButtonPressed(),),

          ],
        ),
      ),
    ),
  );
}