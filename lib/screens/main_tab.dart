import 'package:flutter/material.dart';
import 'package:medico_fast/blocs/api/api_bloc.dart';
import 'package:medico_fast/blocs/home/bloc.dart';
import 'package:medico_fast/blocs/labs/bloc.dart';
import 'package:medico_fast/blocs/login/bloc.dart';
import 'package:medico_fast/screens/cart.dart';
import 'package:medico_fast/screens/home.dart';
import 'package:medico_fast/screens/lab.dart';
import 'package:medico_fast/screens/login.dart';
import 'package:medico_fast/screens/notifications.dart';
import 'package:medico_fast/screens/profile.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MainTab extends StatelessWidget {
  MainTab({Key key}) : super(key: key);

  PersistentTabController _controller = PersistentTabController(initialIndex: 0);

   List<Widget> _buildScreens() {
     return [
       MultiBlocProvider(
         providers: [
           BlocProvider<ApiBloc>(  create: (context) => ApiBloc()),
           BlocProvider<HomeBloc>(  create: (context) => HomeBloc()),
         ],
         child:new HomePage(),
       ),
       MultiBlocProvider(
         providers: [
           BlocProvider<ApiBloc>(  create: (context) => ApiBloc()),
           BlocProvider<LabBloc>(  create: (context) => LabBloc()),
         ],
         child:new Lab(),
       ),
       CartPage(),
       NotificationPage(),
       Profile(),
     ];
   }
   List<PersistentBottomNavBarItem> _navBarsItems() {
     return [
       PersistentBottomNavBarItem(
         icon: Icon(Icons.home),
         title: ("Home"),
         activeColorPrimary: Colors.white,
         inactiveColorPrimary: Colors.black.withOpacity(0.5),
       ),
       PersistentBottomNavBarItem(
         icon: Icon(Icons.event_available),
         title: ("Labs"),
         activeColorPrimary:Colors.white,
         inactiveColorPrimary: Colors.black.withOpacity(0.7),
       ),
       PersistentBottomNavBarItem(
         icon: Icon(Icons.shopping_cart),
         title: ("Cart"),
         activeColorPrimary:Colors.white,
         inactiveColorPrimary: Colors.black.withOpacity(0.7),
       ),
       PersistentBottomNavBarItem(
         icon: Icon(Icons.notification_important),
         title: ("Notifications"),
         activeColorPrimary:Colors.white,
         inactiveColorPrimary: Colors.black.withOpacity(0.7),
       ),
       PersistentBottomNavBarItem(
         icon: Icon(Icons.account_circle_rounded),
         title: ("Profile"),
         activeColorPrimary:Colors.white,
         inactiveColorPrimary: Colors.black.withOpacity(0.7),
         routeAndNavigatorSettings:RouteAndNavigatorSettings(
           // initialRoute: '/',
           routes: {
             '/login':  (context) => BlocProvider<LoginBloc>(
               create: (context) => LoginBloc(),
               child: Login(),
             ),
           },
         ),
       ),
     ];
   }

  @override
  Widget build(BuildContext context) {
    return PersistentTabView(
      context,
      controller: _controller,
      screens: _buildScreens(),
      items: _navBarsItems(),
      confineInSafeArea: true,
      backgroundColor:AppColors.PRIMARY_COLOR, // Default is Colors.white.
      handleAndroidBackButtonPress: true, // Default is true.
      resizeToAvoidBottomInset: true, // This needs to be true if you want to move up the screen when keyboard appears. Default is true.
      stateManagement: true, // Default is true.
      hideNavigationBarWhenKeyboardShows: true, // Recommended to set 'resizeToAvoidBottomInset' as true while using this argument. Default is true.
      decoration: NavBarDecoration(
        borderRadius: BorderRadius.circular(10.0),
        colorBehindNavBar: Colors.white,
      ),
      popAllScreensOnTapOfSelectedTab: true,
      popActionScreens: PopActionScreensType.all,
      itemAnimationProperties: ItemAnimationProperties( // Navigation Bar's items animation properties.
        duration: Duration(milliseconds: 200),
        curve: Curves.ease,
      ),
      screenTransitionAnimation: ScreenTransitionAnimation( // Screen transition animation on change of selected tab.
        animateTabTransition: true,
        curve: Curves.ease,
        duration: Duration(milliseconds: 200),
      ),
      navBarStyle: NavBarStyle.style6, // Choose the nav bar style with this property.

    );
  }
}