import 'package:flutter/material.dart';
import 'package:medico_fast/models/product.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/assets.dart';
import 'package:medico_fast/widgets/back_header.dart';
import 'package:medico_fast/widgets/bottom_view.dart';
import 'package:medico_fast/widgets/product_card.dart';



class Products extends StatefulWidget {
  @override
  _ProductsState createState() => _ProductsState();
}

class _ProductsState extends State<Products> {
  List<Product> products = [
    Product(id: '1',name:'Accu-Check Glumotere Test Strip (Pack of 50)',imageURL: AssetManager.product.value,price: '100',discount: '10',title: 'By general Chemist'),
    Product(id: '2',name:'Ayurvedic',imageURL: AssetManager.product.value,price: '100',discount: '10',title: 'By general Chemist'),
    Product(id: '3',name:'Lab Test',imageURL: AssetManager.product.value,price: '100',discount: '10',title: 'By general Chemist'),
  ];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.SECONDARY_COLOR,
      appBar: BackHeader(text: 'Products',),
      body: SafeArea(
        child: Stack(
          //fit: StackFit.expand,
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 50),
              child: Column(
                children: [
                  Container(
                    //color: Colors.white,
                    margin: EdgeInsets.all(10),
                    child:  ListView.separated(
                      separatorBuilder: (BuildContext context, int index) => Divider(height: 1),
                      itemCount: products.length,
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      itemBuilder: (context, int i) => ProductCard(product: products[i],),

                    ),

                  ),
                ],
              ),
            ),
            BottomView(
              leftIcon: Icons.filter_alt_outlined,
              leftText: 'Filters',
              rightIcon: Icons.sort,
              rightText: 'Sort',
            )
          ],
        ),
      ),
    );
  }
}
