import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:medico_fast/blocs/register/bloc.dart';
import 'package:medico_fast/blocs/register/event.dart';
import 'package:medico_fast/blocs/register/register_bloc.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/assets.dart';
import 'package:medico_fast/utils/strings.dart';
import 'package:medico_fast/utils/styles.dart';
import 'package:medico_fast/utils/utility.dart';
import 'package:medico_fast/utils/validators.dart';
import 'package:medico_fast/widgets/custom_button.dart';
import 'package:medico_fast/widgets/custom_text_field.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'dart:io' show Platform;

import 'package:medico_fast/widgets/progress_bar.dart';

class SignUp extends StatefulWidget {
  SignUp({Key key}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp>{


  bool isChecked = false;
  String deviceToken = "";
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  RegisterBloc _registerBloc;

  @override
  void initState()  {
    super.initState();
    //rest code here
    _registerBloc = BlocProvider.of<RegisterBloc>(context);
  }

  @override
  void dispose() {
    //rest code here
    _registerBloc.close();
    super.dispose();
  }

  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();


  _onSignButtonPressed() {
    //dismiss keyboard
    FocusScope.of(context).requestFocus(FocusNode());
    //Navigator.of(context).pushNamed('/otp');
    var email = _emailController.text;
    var name = _nameController.text;
    var phone = _phoneController.text;
    var password = _passwordController.text;


    if(!Validators.validateName(name)) Utility.showMessage('Enter Name');
    else if(!Validators.validateField(email)) Utility.showMessage('Enter Email');
    else if(!Validators.validateEmail(email)) Utility.showMessage('Enter Valid Email');
    else if(!Validators.validateField(phone)) Utility.showMessage('Enter Phone Number');
    else if(!Validators.validatePhone(phone)) Utility.showMessage('Phone Number Must Be Between 10 Digits');
    else if(!Validators.validateField(password)) Utility.showMessage('Enter Password');
    else if(!Validators.validatePassword(password)) Utility.showMessage('Password Must Be Minimum 6 Digits');


    else  _registerBloc.add(
          RegisterSubmit(
            name: name,
            email: email,
            password: password,
            mobile: phone,
            deviceToken: '24e21eqsds',
            deviceType: Platform.isIOS ? '2' : '1'
          )
        //,token:deviceToken, platform:Platform.isIOS ? '2' : '1'
      );


  }


  @override
  Widget build(BuildContext context) {
    // print(_loginBloc.state.toString());
    return  Scaffold(
        key: _scaffoldKey,
       // backgroundColor: AppColors.PRIMARY_COLOR,
        body:  BlocListener<RegisterBloc, RegisterState>(
          listener: (context, state) {
            if (state is RegisterError) {
              Utility.showMessage(state.error,error: true);
              //Navigator.pushNamed(context,'/home');
            }
            if (state is RegisterSuccess){
              var response = state.jsonResponse;
              var user = response['user'];
              Navigator.pushNamed(context,'/otp',arguments: {
                'route': 'register',
                'id':user['_id'],
                'name':user['name'],
                'email':user['email'],
                'token':user['unique_token']
              });
            }
          },
          child: _authForm(),
        )

    );
  }

  Widget _authForm() => Stack(
    fit: StackFit.expand,
    children: [
      Container(
        decoration: new BoxDecoration(
            image: new DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage(AssetManager.backgroundImage.value),
            )),
      ),
       _registerForm(),
        BlocBuilder<RegisterBloc, RegisterState>(
          builder: (context, state) {
            if (state is RegisterLoading)  return  ProgressBar();
            return Container();
          },
        )
    ],
  );


  Widget getSocialButton(String imageName) =>Align(
    alignment: Alignment.center,
    child:  InkWell(
      child: Container(
        height: Utility.getDeviceHeight(context) * 0.11,
        width:  Utility.getDeviceHeight(context) * 0.11,
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: AppColors.SECONDARY_COLOR,
          borderRadius: BorderRadius.circular(6),
          border: Border.all(
            color: Colors.grey, //                   <--- border color
            width: 1.0,
          ),
        ),
        child: Center(
          child: Image.asset(imageName,height: 30,width: 30,),
        ),
      ),
    ),
  );

  Widget _registerForm() => SafeArea(
    child: Center(
      child:  SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(height: 30,),
            Image.asset(
              AssetManager.logo.value,
              width: MediaQuery.of(context).size.width * 0.37,
              fit: BoxFit.fill,
              height: Utility.getDeviceHeight(context) * 0.18,
            ),
            const SizedBox(height: 15,),
            const Text(Strings.appName,style: AppStyles.appNameStyle,),
            const SizedBox(height: 15,),
            const SizedBox(height: 0,),
            const SizedBox(height: 20,),
            CustomTextField(
              label: 'Name',
              controller: _nameController,
            ),
            CustomTextField(
              label: 'Phone No.',
              controller: _phoneController,
            ),
            CustomTextField(
              label: 'Email',
              inputType: TextInputType.emailAddress,
              controller: _emailController,
            ),
            CustomTextField(
              label: 'Password',
              obscureText: true,
              controller: _passwordController,
            ),
            Align(
              alignment: Alignment.centerRight,
              child:  GestureDetector(
                onTap: ()=>  Navigator.pushNamed(context,'/forgot_password'),
                child: Text("Forgot Password ?     ",style: TextStyle(color:Colors.white,fontSize: 18),),
              ),
            ),
            SizedBox(height: 10,),
            CustomButton(text: 'Sign Up',onPressed: ()=> {
              // Navigator.pushNamed(context,'/home')
              _onSignButtonPressed()
            },),
            SizedBox(height: 15,),
            // Text('${Strings.alreadyHaveAccount}?',style: AppStyles.mediumTextStyle,),
            SizedBox(height: 10,),
            
          ],
        ),
      ),
    ),
  );



}
