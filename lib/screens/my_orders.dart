import 'package:flutter/material.dart';
import 'package:medico_fast/models/orders.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/assets.dart';
import 'package:medico_fast/utils/utility.dart';
import 'package:medico_fast/widgets/back_header.dart';
import 'package:medico_fast/widgets/orders_card.dart';



class Delivered extends StatefulWidget {
  @override
  _DeliveredState createState() => _DeliveredState();
}

class _DeliveredState extends State<Delivered> {
  List<Orders> orders = [
   Orders(id: '1',name: 'Accu Waether preessure check meter',date: 'Delivered 10 days ago',imageURL: AssetManager.product.value),
    Orders(id: '1',name: 'Accu Waether preessure check meter',date: 'Delivered 10 days ago',imageURL: AssetManager.product.value),
    Orders(id: '1',name: 'Accu Waether preessure check meter',date: 'Delivered 10 days ago',imageURL: AssetManager.product.value)

  ];



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.SECONDARY_COLOR,
      body: Container(
        //color: Colors.white,
        margin: EdgeInsets.all(10),
        child:  ListView.separated(
          separatorBuilder: (BuildContext context, int index) => Divider(height: 1),
          itemCount: orders.length,
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          scrollDirection: Axis.vertical,
          itemBuilder: (context, int i) => OrdersCard(orders: orders[i],),

        ),

      ),
    );
  }
}

class MyOrders extends StatefulWidget {
  @override
  _MyOrdersState createState() => _MyOrdersState();
}

class _MyOrdersState extends State<MyOrders>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(length: 4, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
    //  backgroundColor:AppColors.WHITE_COLOR,
      appBar: BackHeader(text: 'My Orders',),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              // give the tab bar a height [can change hheight to preferred height]

              // tab bar view here
              Expanded(
                child: TabBarView(
                  controller: _tabController,

                  children: [
                    // first tab bar view widget
                   Delivered(),
                    // second tab bar view widget
                    Delivered(),
                    Delivered(),
                    Delivered(),

                  ],
                ),
              ),

              Container(
                height: 45,
                decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.circular(
                    5.0,
                  ),
                ),
                child: TabBar(
                  controller: _tabController,
                  // give the indicator a decoration (color and border radius)
                  indicatorColor: Colors.transparent,
                  indicator: BoxDecoration(
                    borderRadius: BorderRadius.circular(
                      5.0,
                    ),
                    color: AppColors.PRIMARY_COLOR,
                  ),
                  labelColor: Colors.white,
                  unselectedLabelColor: Colors.black,
                  tabs: [
                    // first tab [you can add an icon using the icon property]
                    Tab(
                      text: 'Place Bid',
                    ),

                    // second tab [you can add an icon using the icon property]
                    Tab(
                      text: 'Buy Now',
                    ),
                    Tab(
                      text: 'Place Bid',
                    ),

                    // second tab [you can add an icon using the icon property]
                    Tab(
                      text: 'Buy Now',
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}