import 'package:flutter/material.dart';
import 'package:medico_fast/models/address.dart';
import 'package:medico_fast/models/notification.dart';
import 'package:medico_fast/models/orders.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/assets.dart';
import 'package:medico_fast/utils/utility.dart';
import 'package:medico_fast/widgets/address_card.dart';
import 'package:medico_fast/widgets/back_header.dart';
import 'package:medico_fast/widgets/image_button.dart';
import 'package:medico_fast/widgets/image_header.dart';
import 'package:medico_fast/widgets/notification_card.dart';
import 'package:medico_fast/widgets/orders_card.dart';



class NotificationPage extends StatefulWidget {
  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  List<NotificationModel> notifications = [
    NotificationModel(id: '1',title: 'Blue accu check has been out for delivery as of now',time: '2 days'),
    NotificationModel(id: '1',title: 'Blue accu check has been out for delivery as of now',time: '3 days'),
  ];



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.SECONDARY_COLOR,
      appBar: ImageAppBar(text: 'Notifications',icon: Icons.notifications,),
      body: Column(
        children: [
          Container(
            //color: Colors.white,
            margin: EdgeInsets.all(10),
            child:  ListView.separated(
              separatorBuilder: (BuildContext context, int index) => Divider(height: 1),
              itemCount: notifications.length,
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              scrollDirection: Axis.vertical,
              itemBuilder: (context, int i) => NotificationCard(notification: notifications[i],),

            ),

          ),

        ],
      ),
    );
  }
}
