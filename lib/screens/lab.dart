import 'package:flutter/material.dart';
import 'package:medico_fast/blocs/labs/bloc.dart';
import 'package:medico_fast/blocs/labs/event.dart';
import 'package:medico_fast/blocs/labs/lab_bloc.dart';
import 'package:medico_fast/models/medicine.dart';
import 'package:medico_fast/models/packages.dart';
import 'package:medico_fast/screens/products.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/assets.dart';
import 'package:medico_fast/utils/styles.dart';
import 'package:medico_fast/utils/utility.dart';
import 'package:medico_fast/widgets/home_header.dart';
import 'package:medico_fast/widgets/carousel.dart';
import 'package:medico_fast/widgets/medicine_card.dart';
import 'package:medico_fast/widgets/packages_card.dart';
import 'package:medico_fast/widgets/progress_bar.dart';
import 'package:medico_fast/widgets/search_bar.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


class Lab extends StatefulWidget {
  Lab({Key key}) : super(key: key);

  @override
  _LabState createState() => _LabState();
}

class _LabState extends State<Lab> {
  LabBloc _labBloc;





  @override
  void initState()  {
    super.initState();
    //rest code here
    _labBloc = BlocProvider.of<LabBloc>(context);
    _labBloc.add(GetLabData());

  }

  @override
  void dispose() {
    _labBloc.close();
    super.dispose();

  }





  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.SECONDARY_COLOR,
      appBar: HomeAppBar(),
      body:BlocListener<LabBloc, LabState>(
          listener: (context, state) {
            if (state is LabError) {
              Utility.showMessage(state.error,error: true);
            }
          },
          child:Stack(
            fit: StackFit.expand,
            children: [
              _buildBody(),
              _buildLoader()
            ],
          )
      ),
    );
  }

  Widget _buildBody() =>SingleChildScrollView(
    child: Column(

      children: [
        SearchBar(),
        //location view
        Container(
          height: 50,
          color: AppColors.PRIMARY_COLOR,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(),
              Row(
                children: [
                  Icon(Icons.location_on,color: Colors.white,),
                  SizedBox(width: 10,),
                  Text('D-22 Lalita Park Laxminagar New Delhi',style:AppStyles.whiteText,)
                ],
              ),
              Text('Change'.toUpperCase(),style:AppStyles.whiteBoldText)
            ],
          ),
        ),
        BlocBuilder<LabBloc, LabState>(
          builder: (context, state) {
            if (state is LabData) return  Carousel(type: 2,bannersList: state.bannersList,);
            return Container();
          },
        ),
        Container(
          child: Row(
            children: [
              Expanded(
                child: Card(
                  margin:EdgeInsets.all(10),
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                    child: Row(
                      children: [
                        Image.asset(AssetManager.flask.value,height: 25,width: 25,),
                        SizedBox(width: 10,),
                        Text('All Test',style: AppStyles.blackText,)
                      ],
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Card(
                  margin:EdgeInsets.all(10),
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                    child: Row(
                      children: [
                        Image.asset(AssetManager.heart.value,height: 25,width: 25,),
                        SizedBox(width: 10,),
                        Text('Health Packages',style: AppStyles.blackText,)
                      ],
                    ),
                  ),
                ),
              ),

            ],
          ),
        ),
        SizedBox(height: 20,),

        Container(
          padding: EdgeInsets.all(10),
          child:  Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Popular Packages',style: AppStyles.blackMediumText,),
              Text('View all'.toUpperCase(),style: AppStyles.greenText,)
            ],
          ),
        ),

        SizedBox(height: 10,),

        BlocBuilder<LabBloc, LabState>(
          builder: (context, state) {
            if (state is LabData) return    Container(
                height: 230,
                padding: EdgeInsets.symmetric(vertical: 10),
                child: ListView.builder(
                  itemCount: state.packages.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, int i) => PackageCard(packages: state.packages[i],),
                )
            );
            //else if (state is HomeLoading)  return  ProgressBar();
            return Container();
          },
        ),




      ],
    ),
  );

  Widget _buildLoader() =>  BlocBuilder<LabBloc, LabState>(
    builder: (context, state) {
      if (state is LabLoading)  return  ProgressBar();
      return Container();
    },
  );
}