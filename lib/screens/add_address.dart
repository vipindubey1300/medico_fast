import 'package:flutter/material.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/widgets/custom_button.dart';
import 'package:medico_fast/widgets/custom_map.dart';
import 'package:medico_fast/widgets/custom_text_field.dart';


class AddAddress extends StatefulWidget {
  @override
  _AddAddressState createState() => _AddAddressState();
}

class _AddAddressState extends State<AddAddress> {

  TextEditingController _locationController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.SECONDARY_COLOR,
      body: SafeArea(
        child: Stack(
          children: [
            Column(
              children: [
                Expanded(
                  flex: 8,
                  child: Container(
                    child: CustomMapView(
                        onGetAddress: (addr) => _locationController.text = addr
                    ),
                  ),
                ),
                Expanded(
                  flex: 4,
                  child: Card(
                    color: AppColors.SECONDARY_COLOR,
                    child:SingleChildScrollView(
                      child:  Container(
                        padding: EdgeInsets.all(10),
                        child: Column(
                          children: [
                            CustomTextField(
                              label: 'Name',
                            ),
                            SizedBox(height: 10,),
                            CustomTextField(
                              label: 'Phone Number',
                            ),
                            SizedBox(height: 10,),
                            CustomTextField(
                              label: 'Full Address',
                              inputType: TextInputType.multiline,
                              controller: _locationController,
                            ),
                            SizedBox(height: 20,),
                            CustomButton(text: 'Add Address')
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
            Positioned(
              top: 20,
              left: 20,
              child: IconButton(
                color: Colors.black,
                padding: EdgeInsets.all(10),
                onPressed: ()=> Navigator.of(context).pop(),
                splashColor: Colors.green,
                icon: Icon(Icons.arrow_back_ios),
              ),
            )
          ],
        ),
      ),
    );
  }
}