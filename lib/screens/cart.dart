import 'package:flutter/material.dart';
import 'package:medico_fast/models/product.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/assets.dart';
import 'package:medico_fast/utils/styles.dart';
import 'package:medico_fast/utils/utility.dart';
import 'package:medico_fast/widgets/cart_card.dart';
import 'package:medico_fast/widgets/image_header.dart';
import 'package:dotted_border/dotted_border.dart';


class CartPage extends StatefulWidget {
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {

  List<Product> products = [
    Product(id: '1',name:'Accu-Check Glumotere Test Strip (Pack of 50)',imageURL: AssetManager.product.value,price: '100',discount: '10',title: 'By general Chemist'),
    Product(id: '2',name:'Ayurvedic',imageURL: AssetManager.product.value,price: '100',discount: '10',title: 'By general Chemist'),
    Product(id: '3',name:'Lab Test',imageURL: AssetManager.product.value,price: '100',discount: '10',title: 'By general Chemist'),
  ];


  Widget deliveryWidget() => Container(
    padding: EdgeInsets.symmetric(horizontal: 10,vertical: 0),
    margin: EdgeInsets.all(10),
    decoration: BoxDecoration(
        border: Border.all(width: 0.5,color: Colors.grey),
        borderRadius: BorderRadius.all(Radius.circular(10))
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            Text('Deliver to :',style: AppStyles.greenTextBold,),
            const SizedBox(width: 6,),
            Text('Expressway New Delhi')
          ],
        ),
        IconButton(
          icon: Icon(Icons.arrow_drop_down),
          iconSize: 30,
          onPressed: () {addressModal();},
          splashColor: Colors.green,
        ),
      ],
    ),
  );

  Widget couponWidget() => Container(
    padding: EdgeInsets.symmetric(horizontal: 10,vertical: 0),
    margin: EdgeInsets.symmetric(horizontal: 0,vertical: 10),
    decoration: BoxDecoration(
        border: Border.all(width: 0.5,color:AppColors.PRIMARY_COLOR),
        borderRadius: BorderRadius.all(Radius.circular(12))
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            Icon(Icons.monetization_on,color: AppColors.PRIMARY_COLOR,),
            const SizedBox(width: 10,),
            Text('Apply Coupons',style: AppStyles.greenText,),
          ],
        ),
        IconButton(
          icon: Icon(Icons.arrow_forward_ios_rounded,color: AppColors.PRIMARY_COLOR,size: 20,),
          iconSize: 30,
          onPressed: () {addressModal();},
          splashColor: Colors.green,
        ),
      ],
    ),
  );

  Widget priceWidget() => Container(
    color: Colors.white,
    padding: EdgeInsets.symmetric(horizontal: 7,vertical: 10),
    child:Column(
      children: [
        couponWidget(),
        SizedBox(height: 5,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
              Text("CART VALUE",style: AppStyles.greyText,),
            Row(
              children: [
                Text('MRP ',style: AppStyles.greyText),
                Text('\$23',style: TextStyle(decoration: TextDecoration.lineThrough,color: Colors.grey),),
                Text(' \$122',style: AppStyles.blackText)
              ],
            )
          ],
        ),
        SizedBox(height: 15,),
        Divider(),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("Amount to be paid",style: AppStyles.blackBoldText),
            Text('\$122',style: AppStyles.blackBoldText,)
          ],
        ),
        SizedBox(height: 15,),
        DottedBorder(
          color:AppColors.PRIMARY_COLOR,
          strokeWidth: 1,
          radius: Radius.circular(12),
          child:ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(12)),
            child: Container(
              color: AppColors.SECONDARY_COLOR,
              padding: EdgeInsets.all(10),
              child: Row(
                children: [
                  Icon(Icons.monetization_on,color: AppColors.PRIMARY_COLOR,),
                  SizedBox(width: 15,),
                  Text("Total Savings of 22 on this order.",style: AppStyles.greenText),
                ],
              ),
            ),
          ),
        )

      ],
    ),
  );

  void addressModal() {
     showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content:Container(
              height: Utility.getDeviceHeight(context) * 0.5,
              width: Utility.getDeviceWidth(context) * 0.7,
              child:  Stack(
                overflow: Overflow.visible,
                children: <Widget>[
                  Positioned(
                    right: -40.0,
                    top: -40.0,
                    child: InkResponse(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: CircleAvatar(
                        child: Icon(Icons.close),
                        backgroundColor: AppColors.PRIMARY_COLOR,
                        foregroundColor: Colors.white,
                      ),
                    ),
                  ),
                  Container(),
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.SECONDARY_COLOR,
      appBar: ImageAppBar(text: 'Cart',icon: Icons.shopping_cart,),
      body: SafeArea(
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.only(bottom: 60),
              child: SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      deliveryWidget(),
                      const SizedBox(height: 20,),
                      Container(
                        color: Colors.white,
                        child: ListView.separated(
                          separatorBuilder: (BuildContext context, int index) => Divider(height: 1),
                          itemCount: products.length,
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          scrollDirection: Axis.vertical,
                          itemBuilder: (context, int i) => CartCard(product: products[i],),

                        ),
                      ),//llsit view
                      const SizedBox(height: 14,),
                      priceWidget()

                    ],
                  ),
                ),
              ),
            ),
            bottomWidget()
          ],
        ),
      ),
    );
  }



  Widget bottomWidget() => Positioned(
    bottom: 10,right: 0,left: 0,
    child: Container(
      height: 60,
      child: Card(
        elevation: 1,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 15,vertical: 1),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(width: 100,child: ElevatedButton(
                onPressed: (){},
                child: Row(
                  children: [
                    Text('Pay',style: AppStyles.whiteText,),
                    SizedBox(width: 10,),
                    Text('12',style: AppStyles.whiteMediumText,),
                  ],
                ),
                style: ElevatedButton.styleFrom(
                  primary: AppColors.PRIMARY_COLOR,
                ),
              ),),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 30,vertical: 5),
                height: 40,
                decoration: AppStyles.boxDecoration,
                child: Row(
                  children: [
                    Text("Payment method  ",style: AppStyles.greyText,),
                    SizedBox(width: 10,),
                    VerticalDivider(color: Colors.black,),
                    SizedBox(width: 10,),
                    Image.asset(AssetManager.google.value,height: 20,width: 20,)
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    ),
  );
}

/**
    InkWell(
    customBorder: new CircleBorder(),
    onTap: () {},
    splashColor: Colors.red,
    child: new Icon(
    Icons.arrow_back,
    size: 24,
    color: Colors.black,
    ),
    */