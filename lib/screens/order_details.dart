import 'package:flutter/material.dart';
import 'package:medico_fast/utils/app_colors.dart';
import 'package:medico_fast/utils/assets.dart';
import 'package:medico_fast/utils/styles.dart';
import 'package:medico_fast/widgets/back_header.dart';
import 'package:timelines/timelines.dart';


class OrderDetails extends StatefulWidget {
  @override
  _OrderDetailsState createState() => _OrderDetailsState();
}

class _OrderDetailsState extends State<OrderDetails> {

  Widget orderTimeLine() {
    return Container(
      decoration: BoxDecoration(color: Colors.white),
      margin: EdgeInsets.only(
        bottom: 10,
      ),
      padding: EdgeInsets.only(
        top: 10,
        left: 10,
        bottom:10,
      ),
      child: Column(
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              width: 90,
              decoration: BoxDecoration(
                  color: AppColors.PRIMARY_COLOR,
                  borderRadius: BorderRadius.all(Radius.circular(7)),
                  border: Border.all(color: AppColors.PRIMARY_COLOR)
              ),
              padding: EdgeInsets.symmetric(horizontal: 11,vertical: 5),
              child: Center(
                child: Text('Delivered',style: AppStyles.whiteText,),
              ),
            ),
          ),
          SizedBox(height: 30,),
          timelineRow("Order Confirmed","12 July 2020"),
          timelineRow("Order Inprocess", "13 Jun 2020"),
          timelineRow("Order Processed", "12 April 2000"),
          timelineRow("Order Shipped", "19 April 2001"),
          timelineLastRow("Order Delivered", "12 May 2020"),
        ],
      ),
    );
  }

  Widget timelineRow(String title, String subTile) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(
          flex: 1,
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                width: 18,
                height: 18,
                decoration: new BoxDecoration(
                  color: Colors.green,
                  shape: BoxShape.circle,
                ),
                child: Text(""),
              ),
              Container(
                width: 3,
                height: 70,
                decoration: new BoxDecoration(
                  color: Colors.green,
                  shape: BoxShape.rectangle,
                ),
                child: Text(""),
              ),
            ],
          ),
        ),
        Expanded(
          flex: 9,
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('${title}',
                  style: TextStyle(
                      fontFamily: "regular",
                      fontSize: 15,
                      color: Colors.black,fontWeight: FontWeight.w600)),
              SizedBox(height: 7,),
              Text('${subTile}',
                  style: TextStyle(
                      fontFamily: "regular",
                      fontSize: 12,
                      color: Colors.black54)),

            ],
          ),
        ),
      ],
    );
  }
  Widget timelineLastRow(String title, String subTile) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Expanded(
          flex: 1,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                width: 18,
                height: 18,
                decoration: new BoxDecoration(
                  color: Colors.green,
                  shape: BoxShape.circle,
                ),
                child: Text(""),
              ),
              Container(
                width: 3,
                height: 20,
                decoration: new BoxDecoration(
                  color: Colors.transparent,
                  shape: BoxShape.rectangle,
                ),
                child: Text(""),
              ),
            ],
          ),
        ),
        Expanded(
          flex: 9,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('${title}',
                  style: TextStyle(
                      fontFamily: "regular",
                      fontSize: 15,
                      color: Colors.black,fontWeight: FontWeight.w600)),
              SizedBox(height: 7,),
              Text('${subTile}',
                  style: TextStyle(
                      fontFamily: "regular",
                      fontSize: 12,
                      color: Colors.black54)),
            ],
          ),
        ),
      ],
    );
  }


  Widget productWidget() => Container(
    color: Colors.white,
    padding: EdgeInsets.all(10),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Order ID 2437438434',style: AppStyles.greenTextBold,),
        SizedBox(height: 15,),
        Row(
          children: [
            Expanded(
              flex: 2,
              child: Image.asset(AssetManager.product.value),
            ),
            Expanded(
              flex: 8,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Accu-Check Active Glucometer Test Strips Box of 50',style: AppStyles.blackBoldText,),
                  SizedBox(height: 8,),
                  Text('50 Glucometer Pack',style: AppStyles.blackText,),
                  SizedBox(height: 8,),
                  Text('Quantity : 1',style: AppStyles.greyText,),
                  SizedBox(height: 8,),
                  Text('Sold By : Ramesh Traders Delhi',style: AppStyles.greyText,),
                ],
              ),
            )
          ],
        )
      ],
    ),
  );

  Widget priceWidget() => Container(
    color: Colors.white,
    padding: EdgeInsets.all(10),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('Price',style: AppStyles.blackText,),
            Text('₹12323',style: AppStyles.blackText)
          ],
        ),
        const SizedBox(height: 7,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('Discount',style: AppStyles.blackText,),
            Text('₹12',style: AppStyles.blackText)
          ],
        ),
        const SizedBox(height: 7,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('Posting and Packaging',style: AppStyles.blackText),
            Text('₹123',style: AppStyles.blackText)
          ],
        ),
        const SizedBox(height: 7,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('Taxes',style: AppStyles.blackText),
            Text('₹12',style: AppStyles.blackText)
          ],
        ),
        const SizedBox(height: 5,),
        const SizedBox(height: 10,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('Total Paid',style: AppStyles.blackMediumText,),
            Text('₹12323',style: AppStyles.blackMediumText)
          ],
        )
      ],
    ),
  );

  Widget returnWidget() => Container(
    color: Colors.white,
    padding: EdgeInsets.all(10),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text("10 Days Remaining",style: AppStyles.blackBoldText,),
        Container(
          height: 40,
          width: 100,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              border: Border.all(color: Colors.red,width: 1.5),

          ),
          child: Center(
            child: Text("Return",style:TextStyle(color:Colors.red,fontWeight: FontWeight.w600,fontSize: 16) ,),
          ),
        )
      ],
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.SECONDARY_COLOR,
      appBar: BackHeader(text: 'Order Details',),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(10),
            child: Column(
              children: [
                productWidget(),
                SizedBox(height: 10,),
                priceWidget(),
                SizedBox(height: 10,),
                returnWidget(),
                SizedBox(height: 10,),
                orderTimeLine()
                
              ],
            ),
          ),
        ),
      ),
    );
  }
}