import 'package:medico_fast/models/banners.dart';
import 'package:medico_fast/models/category.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:medico_fast/models/user.dart';


abstract class HomeState extends Equatable {
  const HomeState();

  @override
  List<Object> get props => [];
}

class HomeInitial extends HomeState {
  @override
  String toString()  =>  "Home Initial";
}


class HomeLoading extends HomeState {
  @override
  String toString()  =>  "Home Initial";
}

class HomeData extends HomeState {
  final List<Banners> bannersList;
  final List<CategoryModel> categoryList;
  HomeData({@required this.bannersList,@required this.categoryList}) ;

  @override
  String toString()  =>  "Home Data";

  @override
  List<Object> get props => [bannersList,categoryList];
}



class HomeError extends HomeState {
  final String error;
  HomeError({@required this.error});

  @override
  String toString()  =>  "Home Error";

  @override
  List<Object> get props => [error];
}


