import 'package:flutter_bloc/flutter_bloc.dart';


// a bloc handler class to show hide bloc
class DialogBloc extends Bloc<bool, bool> {

  DialogBloc() :  super(false);

  @override
  bool get initialState => false;

  @override
  Stream<bool> mapEventToState(bool event) async* {
    yield event;
  }
}