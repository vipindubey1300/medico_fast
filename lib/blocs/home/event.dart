import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'dart:io';


abstract class HomeEvent extends Equatable {
  const HomeEvent();
}

class GetHomeData extends HomeEvent {
  // The data stored in the event.
  GetHomeData();

  @override
  List<Object> get props => [];
}


