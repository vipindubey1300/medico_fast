import 'package:medico_fast/models/banners.dart';
import 'package:medico_fast/models/category.dart';
import 'package:meta/meta.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:medico_fast/blocs/home/home_bloc.dart';
import 'package:medico_fast/controllers/api_exceptions.dart';
import 'package:medico_fast/controllers/api_repository.dart';
import 'package:medico_fast/models/user.dart';
import 'package:medico_fast/utils/constants.dart';
import 'package:medico_fast/utils/storage_helper.dart';
import 'package:flutter/foundation.dart';


class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final repository = ApiRepository();

  HomeBloc() :  super(HomeInitial());



  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {

    if (event is GetHomeData) {
      try {
        yield HomeLoading();

        final  result = await repository.getHomeData(AppConstants.HOME);
        if(result['status']){
         // List<Banners> list = new List<Banners>.from(result['banners']);
          var banners = bannersFromJson(result['banners']);
          var categories = categoriesFromJson(result['categories']);

          yield HomeData(
            bannersList: banners,
            categoryList: categories
          );
        }
        else {
          yield HomeError(error:result['message']);
        }

      }
      on FetchDataException  catch(error){
        print("-----------------"+error.toString());
        yield HomeError(error: error.toString());
      }
      on UnauthorisedException catch(error){
        print("----------------->>>>>>>>>>>"+error.toString());
        yield HomeError(error: error.toString());
      }
      catch (error) {
        print("-----------------!!!!!!!!!!!!"+error.toString());
        yield HomeError(error:"Please try again");
      }
    }


  }
}



