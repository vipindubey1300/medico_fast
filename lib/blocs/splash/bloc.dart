import 'package:bloc/bloc.dart';
import 'package:medico_fast/blocs/splash/event.dart';
import 'package:medico_fast/blocs/splash/splash_bloc.dart';
import 'package:medico_fast/blocs/user/bloc.dart';
import 'package:medico_fast/blocs/splash/state.dart';
import 'package:medico_fast/blocs/user/event.dart';
import 'package:medico_fast/controllers/api_exceptions.dart';
import 'package:medico_fast/controllers/api_repository.dart';
import 'package:medico_fast/models/models.dart';
import 'package:medico_fast/utils/constants.dart';
import 'package:medico_fast/utils/storage_helper.dart';
import 'package:medico_fast/utils/validation_mixin.dart';
import '../blocs.dart';

import 'dart:convert';

import 'package:http/http.dart' as http;
import 'dart:io' show Platform;

class SplashBloc extends Bloc<SplashEvent, SplashState> {

  final userBloc;

  SplashBloc({UserBloc userBloc})
      : assert(userBloc != null),
        userBloc = userBloc,
        super(SplashInitial());



  @override
  Stream<SplashState> mapEventToState(SplashEvent event) async* {
    if (event is SplashSaveUser) {
      yield* _mapSplashWithState(event);
    }


  }



  Stream<SplashState> _mapSplashWithState(SplashSaveUser event) async* {

    try {
      if (true) {
        String id = event.id;
        String name = event.name;
        String email = event.email;
        String token = event.token;

        userBloc.add(UserLoggedIn(
            user: User(id: id, name: name, email: email, token: token)));

      }
    } catch (err) {
      yield SplashError(error: err.message ?? 'An unknown error occured');
    }
  }


}
