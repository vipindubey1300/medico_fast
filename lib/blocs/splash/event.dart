import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class SplashEvent extends Equatable {
  @override
  List<Object> get props => [];
}


class SplashSaveUser extends SplashEvent {
  final String id;
  final String name;
  final String email;
  final String token;

  SplashSaveUser({@required this.id,@required this.name,@required this.email,@required this.token});

  @override
  List<Object> get props => [id,name,email,token];
}


