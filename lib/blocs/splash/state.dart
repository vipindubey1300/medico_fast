import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';


abstract class SplashState extends Equatable {
  const SplashState();
  @override
  List<Object> get props => [];
}


class SplashInitial extends SplashState {
  const SplashInitial();
  @override
  String toString()  =>  "Splash Initial";
}


class SplashError extends SplashState {
  final String error;
  SplashError({@required this.error});

  @override
  String toString()  =>  "Splash Error";

  @override
  List<Object> get props => [error];
}

