import 'package:bloc/bloc.dart';
import 'package:medico_fast/blocs/otp/event.dart';
import 'package:medico_fast/blocs/otp/otp_bloc.dart';
import 'package:medico_fast/blocs/user/bloc.dart';
import 'package:medico_fast/blocs/otp/state.dart';
import 'package:medico_fast/blocs/user/event.dart';
import 'package:medico_fast/controllers/api_exceptions.dart';
import 'package:medico_fast/controllers/api_repository.dart';
import 'package:medico_fast/models/models.dart';
import 'package:medico_fast/utils/constants.dart';
import 'package:medico_fast/utils/storage_helper.dart';
import 'package:medico_fast/utils/validation_mixin.dart';
import '../blocs.dart';

import 'dart:convert';

import 'package:http/http.dart' as http;
import 'dart:io' show Platform;

class OtpBloc extends Bloc<OtpEvent, OtpState> {
  final repository = ApiRepository();

  final userBloc;

  OtpBloc({UserBloc userBloc})
      : assert(userBloc != null),
        userBloc = userBloc,
        super(OtpInitial());



  @override
  Stream<OtpState> mapEventToState(OtpEvent event) async* {
    if (event is OtpSubmit) {
      yield* _mapOtpWithState(event);
    }
    if (event is OtpSaveUser) {
      yield* _mapOtpFinalWithState(event);
    }

  }

  Stream<OtpState> _mapOtpWithState(OtpSubmit event) async* {
    yield OtpLoading();

    try {
      if (true) {
        String otp = event.otp;
        String userId = event.userId;
        final result = await repository.postOtpCall(otp,userId);
        String id = result['id'].toString();
        //String token = result['token'];

        if(result['status']) yield OtpSuccess(jsonResponse: result);
        else yield OtpError(error:result['message']);

        yield OtpInitial();
      }
    } on FetchDataException catch (e) {
      yield OtpError(error: e.toString());
    }  on BadRequestException catch (e) {
      yield OtpError(error: e.toString());
    }
    catch (err) {
      yield OtpError(error: err.message ?? 'An unknown error occured');
    }
  }

  Stream<OtpState> _mapOtpFinalWithState(OtpSaveUser event) async* {
    yield OtpLoading();

    try {
      if (true) {
        String id = event.id;
        String name = event.name;
        String email = event.email;
        String token = event.token;

        await MySharedPreferences.instance.setStringValue('id', id.toString());
        String i = await MySharedPreferences.instance.getStringValue('id');
        print('id is ' + i.toString());

        userBloc.add(UserLoggedIn(
            user: User(id: id, name: name, email: email, token: token)));

        yield OtpUserSaveSuccess();
        yield OtpInitial();
      }
    } on FetchDataException catch (e) {
      yield OtpError(error: e.toString());
    } catch (err) {
      yield OtpError(error: err.message ?? 'An unknown error occured');
    }
  }


}
