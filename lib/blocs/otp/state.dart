import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';


abstract class OtpState extends Equatable {
  const OtpState();
  @override
  List<Object> get props => [];
}


class OtpInitial extends OtpState {
  const OtpInitial();
  @override
  String toString()  =>  "OTP Initial";
}

class OtpLoading extends OtpState {
  @override
  String toString()  =>  "OTP Loading";
}

class OtpSuccess extends OtpState {
  //when api call is success then pass the success json response to calling method
  final dynamic jsonResponse;
  const OtpSuccess({@required this.jsonResponse}) : assert(jsonResponse != null);
  @override
  List<Object> get props => [jsonResponse];
}

class OtpUserSaveSuccess extends OtpState {
  //when final user is saved after successful otp verification
  @override
  String toString()  =>  "OTP User Save Success";
}


class OtpError extends OtpState {
  final String error;
  OtpError({@required this.error});

  @override
  String toString()  =>  "OTP Error";

  @override
  List<Object> get props => [error];
}