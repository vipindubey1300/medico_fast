import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class OtpEvent extends Equatable {
  @override
  List<Object> get props => [];
}


class OtpSubmit extends OtpEvent {
  final String otp;
  final String userId;
  OtpSubmit({@required this.otp,@required this.userId});

  @override
  List<Object> get props => [otp];
}

class OtpSaveUser extends OtpEvent {
  final String id;
  final String name;
  final String email;
  final String token;

  OtpSaveUser({@required this.id,@required this.name,@required this.email,@required this.token});

  @override
  List<Object> get props => [id,name,email,token];
}


