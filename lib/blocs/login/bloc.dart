import 'package:bloc/bloc.dart';
import 'package:medico_fast/blocs/login/event.dart';
import 'package:medico_fast/blocs/login/login_bloc.dart';
import 'package:medico_fast/blocs/user/bloc.dart';
import 'package:medico_fast/blocs/user/event.dart';
import 'package:medico_fast/controllers/api_exceptions.dart';
import 'package:medico_fast/controllers/api_repository.dart';
import 'package:medico_fast/models/models.dart';
import 'package:medico_fast/utils/constants.dart';
import 'package:medico_fast/utils/storage_helper.dart';
import 'package:medico_fast/utils/validation_mixin.dart';
import '../blocs.dart';

import 'dart:convert';

import 'package:http/http.dart' as http;
import 'dart:io' show Platform;

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final repository = ApiRepository();
  LoginBloc() :  super(LoginInitial());

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginSubmit) {
      yield* _mapLoginWithState(event);
    }

  }

  Stream<LoginState> _mapLoginWithState(LoginSubmit event) async* {
    yield LoginLoading();

    try {
      if (true) {

        String mobile = event.mobile;
        String deviceToken = event.deviceToken;
        String deviceType = event.deviceType;
        final result = await repository.postLoginData( mobile,deviceType,deviceToken);
        if(result['status']) yield LoginSuccess(jsonResponse: result);
        else yield LoginError(error:result['message']);
        yield LoginInitial();
      }
    } on FetchDataException catch (e) {
      print('E'+e.toString());
      yield LoginError(error: e.toString());
    } catch (err) {
      print('E'+err.toString());
      yield LoginError(error: err.message ?? 'An unknown error occurred');
    }
  }



}
