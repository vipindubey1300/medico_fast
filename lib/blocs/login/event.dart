import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class LoginEvent extends Equatable {
  @override
  List<Object> get props => [];
}


class LoginSubmit extends LoginEvent {
  final String mobile;
  final String deviceType;
  final String deviceToken;

  LoginSubmit({
    @required this.mobile,
    @required this.deviceType,
    @required this.deviceToken});

  @override
  List<Object> get props => [mobile,deviceType,deviceToken];
}



class RegisterWithGoogle extends LoginEvent {
}

class RegisterWithFacebook extends LoginEvent {
}
class RegisterWithTwitter extends LoginEvent {
}