import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';


abstract class LoginState extends Equatable {
  const LoginState();
  @override
  List<Object> get props => [];
}


class LoginInitial extends LoginState {
  const LoginInitial();
  @override
  String toString()  =>  "Login Initial";
}

class LoginLoading extends LoginState {
  @override
  String toString()  =>  "Login Loading";
}

class LoginSuccess extends LoginState {
  //when api call is success then pass the success json response to calling method
  final dynamic jsonResponse;
  const LoginSuccess({@required this.jsonResponse}) : assert(jsonResponse != null);
  @override
  List<Object> get props => [jsonResponse];
}

class LoginError extends LoginState {
  final String error;
  LoginError({@required this.error});

  @override
  String toString()  =>  "Login Error";

  @override
  List<Object> get props => [error];
}