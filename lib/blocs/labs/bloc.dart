import 'package:medico_fast/models/banners.dart';
import 'package:medico_fast/models/category.dart';
import 'package:medico_fast/models/packages.dart';
import 'package:meta/meta.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:medico_fast/blocs/labs/lab_bloc.dart';
import 'package:medico_fast/controllers/api_exceptions.dart';
import 'package:medico_fast/controllers/api_repository.dart';
import 'package:medico_fast/models/user.dart';
import 'package:medico_fast/utils/constants.dart';
import 'package:medico_fast/utils/storage_helper.dart';
import 'package:flutter/foundation.dart';


class LabBloc extends Bloc<LabEvent, LabState> {
  final repository = ApiRepository();

  LabBloc() :  super(LabInitial());

  @override
  Stream<LabState> mapEventToState(LabEvent event) async* {

    if (event is GetLabData) {
      try {
        yield LabLoading();
        final  result = await repository.getLabData(AppConstants.LAB_TESTS);
        if(result['status']){
         // List<Banners> list = new List<Banners>.from(result['banners']);
          var banners = bannersFromJson(result['banners']);
          var tests = packagesFromJson(result['labTests']);

          yield LabData(
            bannersList: banners,
            packages: tests
          );
        }
        else {
          yield LabError(error:result['message']);
        }

      }
      on FetchDataException  catch(error){
        print("-----------------"+error.toString());
        yield LabError(error: error.toString());
      }
      on UnauthorisedException catch(error){
        print("----------------->>>>>>>>>>>"+error.toString());
        yield LabError(error: error.toString());
      }
      catch (error) {
        print("-----------------!!!!!!!!!!!!"+error.toString());
        yield LabError(error:"Please try again");
      }
    }


  }
}



