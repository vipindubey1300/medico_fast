import 'package:medico_fast/models/banners.dart';
import 'package:medico_fast/models/category.dart';
import 'package:medico_fast/models/packages.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:medico_fast/models/user.dart';


abstract class LabState extends Equatable {
  const LabState();

  @override
  List<Object> get props => [];
}

class LabInitial extends LabState {
  @override
  String toString()  =>  "LabState Initial";
}


class LabLoading extends LabState {
  @override
  String toString()  =>  "Lab Initial";
}

class LabData extends LabState {
  final List<Banners> bannersList;
  final List<Packages> packages;
  LabData({@required this.bannersList,@required this.packages}) ;

  @override
  String toString()  =>  "LabState Data";

  @override
  List<Object> get props => [bannersList,packages];
}



class LabError extends LabState {
  final String error;
  LabError({@required this.error});

  @override
  String toString()  =>  "Lab Error";

  @override
  List<Object> get props => [error];
}


