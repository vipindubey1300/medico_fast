import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'dart:io';


abstract class LabEvent extends Equatable {
  const LabEvent();
}

class GetLabData extends LabEvent {
  // The data stored in the event.
  GetLabData();

  @override
  List<Object> get props => [];
}


