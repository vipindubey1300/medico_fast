import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:medico_fast/models/models.dart';


class UserState  extends Equatable {
  final User user;

  UserState({
    this.user 
  });

  @override
  List<Object> get props => [user];
}
