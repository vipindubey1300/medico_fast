import 'package:bloc/bloc.dart';
import 'package:medico_fast/blocs/register/event.dart';
import 'package:medico_fast/blocs/register/register_bloc.dart';
import 'package:medico_fast/blocs/user/bloc.dart';
import 'package:medico_fast/blocs/user/event.dart';
import 'package:medico_fast/controllers/api_exceptions.dart';
import 'package:medico_fast/controllers/api_repository.dart';
import 'package:medico_fast/models/models.dart';
import 'package:medico_fast/utils/constants.dart';
import 'package:medico_fast/utils/storage_helper.dart';
import 'package:medico_fast/utils/validation_mixin.dart';
import '../blocs.dart';

import 'dart:convert';

import 'package:http/http.dart' as http;
import 'dart:io' show Platform;

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final repository = ApiRepository();


  RegisterBloc() :  super(RegisterInitial());



  @override
  Stream<RegisterState> mapEventToState(RegisterEvent event) async* {
    if (event is RegisterSubmit) {
      yield* _mapRegisterWithState(event);
    }


  }

  Stream<RegisterState> _mapRegisterWithState(RegisterSubmit event) async* {
    yield RegisterLoading();

    try {
      if (true) {
        String email = event.email;
        String password = event.password;
        String mobile = event.mobile;
        String name = event.name;
        String deviceToken = event.deviceToken;
        String deviceType = event.deviceType;

        final result = await repository.postSignUpData(
            email, password, name, mobile,deviceType,deviceToken);
        if(result['status']) yield RegisterSuccess(jsonResponse: result);
        else yield RegisterError(error:result['message']);

        yield RegisterInitial();
      }
    } on FetchDataException catch (e) {
      yield RegisterError(error: e.toString());
    } catch (err) {
      yield RegisterError(error: err.message ?? 'An unknown error occured');
    }
  }



}
