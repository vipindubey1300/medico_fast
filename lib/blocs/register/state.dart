import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';


abstract class RegisterState extends Equatable {
  const RegisterState();
  @override
  List<Object> get props => [];
}


class RegisterInitial extends RegisterState {
  const RegisterInitial();
  @override
  String toString()  =>  "Register Initial";
}

class RegisterLoading extends RegisterState {
  @override
  String toString()  =>  "Register Loading";
}

class RegisterSuccess extends RegisterState {
  //when api call is success then pass the success json response to calling method
  final dynamic jsonResponse;
  const RegisterSuccess({@required this.jsonResponse}) : assert(jsonResponse != null);
  @override
  List<Object> get props => [jsonResponse];
}




class RegisterError extends RegisterState {
  final String error;
  RegisterError({@required this.error});

  @override
  String toString()  =>  "Register Error";

  @override
  List<Object> get props => [error];
}