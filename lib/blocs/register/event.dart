import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class RegisterEvent extends Equatable {
  @override
  List<Object> get props => [];
}


class RegisterSubmit extends RegisterEvent {
  final String email;
  final String password;
  final String mobile;
  final String name;
  final String deviceType;
  final String deviceToken;

  RegisterSubmit({
    @required this.email,
    @required this.password,
    @required this.mobile,
    @required this.name,
    @required this.deviceType,
    @required this.deviceToken});

  @override
  List<Object> get props => [email,password,mobile,name,deviceType,deviceToken];
}




class RegisterWithGoogle extends RegisterEvent {
}

class RegisterWithFacebook extends RegisterEvent {
}
class RegisterWithTwitter extends RegisterEvent {
}