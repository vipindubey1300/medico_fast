class Product{
  final String name;
  final String title;
  final String id;
  final String price;
  final String discount;
  final String imageURL;

  Product({
    this.name,
    this.title,
    this.id,
    this.imageURL,
    this.discount,
    this.price

  });
}