import 'dart:convert';
List<Packages> packagesFromJson(List<dynamic> list) => list.map((x) => Packages.fromJson(x)).toList();
String packagesToJson(List<Packages> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Packages {
  String sId;
  String description;
  String price;
  String discount;
  String image;
  String name;
  User user;

  Packages(
      {this.sId,
        this.description,
        this.price,
        this.discount,
        this.image,
        this.name,
        this.user});

  Packages.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    description = json['description'];
    price = json['price'];
    discount = json['discount'];
    image = json['image'];
    name = json['name'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['description'] = this.description;
    data['price'] = this.price;
    data['discount'] = this.discount;
    data['image'] = this.image;
    data['name'] = this.name;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}

class User {
  String sId;
  String name;

  User({this.sId, this.name});

  User.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['name'] = this.name;
    return data;
  }
}