class Address{
  final String id;
  final String phone;
  final String address;

  Address({
    this.id,
    this.phone,
    this.address
  });
}