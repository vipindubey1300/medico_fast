import 'dart:convert';
List<Banners> bannersFromJson(List<dynamic> list) => list.map((x) => Banners.fromJson(x)).toList();

//List<Banners> bannersFromJson(String str) => List<Banners>.from(json.decode(str).map((x) => Banners.fromJson(x)));

String bannersToJson(List<Banners> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Banners {
  Banners({
    this.name,
    this.id,
    this.image,
  });

  String name;
  String id;
  String image;

  factory Banners.fromJson(Map<String, dynamic> json) => Banners(
    id: json["_id"],
    name: json["name"],
    image: json["image"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "image": image,
  };
}