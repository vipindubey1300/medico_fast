
import 'dart:convert';
List<CategoryModel> categoriesFromJson(List<dynamic> list) => list.map((x) => CategoryModel.fromJson(x)).toList();
String categoriesToJson(List<CategoryModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CategoryModel {
  CategoryModel({
    this.name,
    this.id,
    this.image,
  });

  String name;
  String id;
  String image;

  factory CategoryModel.fromJson(Map<String, dynamic> json) => CategoryModel(
    id: json["_id"],
    name: json["name"],
    image: json["image"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "image": image,
  };
}