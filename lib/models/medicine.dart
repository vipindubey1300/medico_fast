class MedicineModel{
  final String title;
  final String id;
  final String avatarURL;

  MedicineModel({
    this.title,
    this.id,
    this.avatarURL
  });
}