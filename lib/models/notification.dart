class NotificationModel{
  final String title;
  final String id;
  final String time;

  NotificationModel({
    this.title,
    this.id,
    this.time
  });
}